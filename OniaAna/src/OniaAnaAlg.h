#ifndef ONIAANA_ONIAANAALG_H
#define ONIAANA_ONIAANAALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

//Example ROOT Includes
#include "TTree.h"
#include "TH1D.h"
#include "TLorentzVector.h"
#include "AsgTools/AnaToolHandle.h"

#include "BLSToolBox/IBLSToolBox.h"


class OniaAnaAlg: public ::AthAnalysisAlgorithm {
	public:
		OniaAnaAlg( const std::string& name, ISvcLocator* pSvcLocator );
		virtual ~OniaAnaAlg();

		///uncomment and implement methods as required

		//IS EXECUTED:
		virtual StatusCode  initialize();     //once, before any input is loaded
		virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
		//virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
		virtual StatusCode  execute();        //per event
		//virtual StatusCode  endInputFile();   //end of each input file
		//virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
		virtual StatusCode  finalize();       //once, after all events processed


		///Other useful methods provided by base class are:
		///evtStore()        : ServiceHandle to main event data storegate
		///inputMetaStore()  : ServiceHandle to input metadata storegate
		///outputMetaStore() : ServiceHandle to output metadata storegate
		///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
		///currentFile()     : TFile* to the currently open input file
		///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


	private:

		asg::AnaToolHandle<BLS::IBLSToolBox> m_toolbox;

		std::string m_bphysVertexKey;

		std::string m_Year;
		bool m_MuonLoop;

		StatusCode bookBranches ();
		StatusCode clearBranches();
		StatusCode fillTree();

		TTree* m_myTree = nullptr;

		//Trigger list for different years
		//Format: pair<Name, wheter it's dimuon trigger or not>
		std::vector<std::pair<std::string,bool>> m_triggerNames;

		int  m_RunNumber;           //!
		int  m_EventNumber;         //!
		float  m_Pileup;            //!
		int  m_LumiBlock;           //!
		int  m_mcChannelNumber;     //!

		/*
		int m_HLT_2mu4_bUpsimumu;   //!
		int m_HLT_2mu4_bJpsimumu;   //!
		int m_HLT_2mu4_bUpsimumu_noL2;   //!
		int m_HLT_2mu4_bJpsimumu_noL2;   //!
		int m_HLT_mu6_mu4_bUpsimumu;//!
		int m_HLT_mu6_mu4_bJpsimumu;//!
		int m_HLT_mu6_mu4_bUpsimumu_noL2;//!
		int m_HLT_mu6_mu4_bJpsimumu_noL2;//!
		int m_HLT_2mu6_bUpsimumu;   //!
		int m_HLT_2mu6_bJpsimumu;   //!
		int m_HLT_2mu6_bUpsimumu_noL2;   //!
		int m_HLT_2mu6_bJpsimumu_noL2;   //!
		int m_HLT_2mu10_bUpsimumu;   //!
		int m_HLT_2mu10_bJpsimumu;   //!
		int m_HLT_2mu10_bUpsimumu_noL2;   //!
		int m_HLT_2mu10_bJpsimumu_noL2;   //!
		int m_HLT_2mu4_bDimu;       //!
		int m_HLT_2mu4_bDimu_noinvm_novtx_ss;       //!
		int m_HLT_2mu4_bDimu_novtx_noos;   //!
		int m_HLT_mu4;                     //!
		int m_HLT_mu4_bJpsi_Trkloose;      //!
		int m_HLT_mu6;                     //!
		int m_HLT_mu6_bJpsi_Trkloose;      //!
		int m_HLT_mu20_L1MU15;             //!
		int m_HLT_mu20_iloose_L1MU15;      //!
		int m_HLT_mu24_ivarmedium;				//!
		int m_HLT_mu26_ivarmedium;				//!
		int m_HLT_mu18_2mu0noL1_JpsimumuFS;	//!
		int m_HLT_mu20_2mu0noL1_JpsimumuFS; //!
		int m_HLT_mu20_2mu2noL1_JpsimumuFS; //!
		*/

		int m_EventTrigger; //!

		std::vector<float>* m_PS;  //!

		/*
		float m_PS_mu4;                    //!
		float m_PS_mu6;                    //!
		float m_PS_mu4_bJpsi_Trkloose;     //!
		float m_PS_mu6_bJpsi_Trkloose;     //!
		float m_PS_2mu4_bJpsimumu;         //!
		float m_PS_2mu4_bUpsimumu;         //!
		float m_PS_2mu4_bJpsimumu_noL2;    //!
		float m_PS_2mu4_bUpsimumu_noL2;    //!
		float m_PS_mu6_mu4_bJpsimumu;      //!
		float m_PS_mu6_mu4_bUpsimumu;      //!
		float m_PS_mu6_mu4_bJpsimumu_noL2; //!
		float m_PS_mu6_mu4_bUpsimumu_noL2; //!
		float m_PS_2mu6_bJpsimumu;         //!
		float m_PS_2mu6_bUpsimumu;         //!
		float m_PS_2mu6_bJpsimumu_noL2;    //!
		float m_PS_2mu6_bUpsimumu_noL2;    //!
		float m_PS_2mu10_bJpsimumu;         //!
		float m_PS_2mu10_bUpsimumu;         //!
		float m_PS_2mu10_bJpsimumu_noL2;    //!
		float m_PS_2mu10_bUpsimumu_noL2;    //!
		float m_PS_mu20_L1MU15;            //!
		float m_PS_mu20_iloose_L1MU15;     //!
		float m_PS_mu24_ivarmedium;				//!
		float m_PS_mu26_ivarmedium;				//!
		float m_PS_mu18_2mu0noL1_JpsimumuFS;	//!
		float m_PS_mu20_2mu0noL1_JpsimumuFS; //!
		float m_PS_mu20_2mu2noL1_JpsimumuFS; //!
		*/

		std::vector<float>*  m_Truth_Parent_PdgId = nullptr; //!
		std::vector<float>*  m_Truth_Parent_Pt = nullptr;    //!
		std::vector<float>*  m_Truth_Parent_Eta = nullptr;   //!
		std::vector<float>*  m_Truth_Parent_Phi = nullptr;   //!
		std::vector<float>*  m_Truth_Parent_Mass = nullptr;  //!
		std::vector<float>*  m_Truth_Onia_PdgId = nullptr;   //!
		std::vector<float>*  m_Truth_Onia_Pt = nullptr;      //!
		std::vector<float>*  m_Truth_Onia_Eta = nullptr;     //!
		std::vector<float>*  m_Truth_Onia_Phi = nullptr;     //!
		std::vector<float>*  m_Truth_Onia_Mass = nullptr;    //!
		std::vector<float>*  m_Truth_Onia_Vertex_X = nullptr;//!
		std::vector<float>*  m_Truth_Onia_Vertex_Y = nullptr;//!
		std::vector<float>*  m_Truth_Onia_Vertex_Z = nullptr;//!
		std::vector<float>*  m_Truth_MuMinus_Pt = nullptr;   //!
		std::vector<float>*  m_Truth_MuMinus_Eta = nullptr;  //!
		std::vector<float>*  m_Truth_MuMinus_Phi = nullptr;  //!
		std::vector<float>*  m_Truth_MuPlus_Pt = nullptr;    //!
		std::vector<float>*  m_Truth_MuPlus_Eta = nullptr;   //!
		std::vector<float>*  m_Truth_MuPlus_Phi = nullptr;   //!
		float m_Truth_PV_Z;         //!
		float m_Truth_PV_X;         //!
		float m_Truth_PV_Y;         //!
		float m_PV_Z;  //!
		float m_PV_X;  //!
		float m_PV_Y;  //!

		std::vector<float>* m_DiMuon_Mass = nullptr;    //!
		std::vector<float>* m_DiMuon_MassErr = nullptr; //!
		std::vector<float>* m_DiMuon_Pt = nullptr;      //!
		std::vector<float>* m_DiMuon_Rapidity = nullptr;//!
		std::vector<float>* m_DiMuon_ChiSq = nullptr;   //!
		std::vector<float>* m_DiMuon_Vertex_X = nullptr;//!
		std::vector<float>* m_DiMuon_Vertex_Y = nullptr;//!
		std::vector<float>* m_DiMuon_Vertex_Z = nullptr;//!
		std::vector<float>* m_Tau_InvMass_MaxSumPt2      = nullptr; //!
		std::vector<float>* m_Tau_JpsiMass_MaxSumPt2     = nullptr; //!
		std::vector<float>* m_TauErr_InvMass_MaxSumPt2   = nullptr; //!
		std::vector<float>* m_TauErr_JpsiMass_MaxSumPt2  = nullptr; //!
		std::vector<float>* m_Tau_InvMass_Min_A0         = nullptr; //!
		std::vector<float>* m_Tau_JpsiMass_Min_A0        = nullptr; //!
		std::vector<float>* m_TauErr_InvMass_Min_A0      = nullptr; //!
		std::vector<float>* m_TauErr_JpsiMass_Min_A0     = nullptr; //!
		std::vector<float>* m_Tau_InvMass_Min_Z0         = nullptr; //!
		std::vector<float>* m_Tau_JpsiMass_Min_Z0        = nullptr; //!
		std::vector<float>* m_TauErr_InvMass_Min_Z0      = nullptr; //!
		std::vector<float>* m_TauErr_JpsiMass_Min_Z0     = nullptr; //!

		std::vector<float>* m_Lxy_MaxSumPt2    = nullptr; //!
		std::vector<float>* m_LxyErr_MaxSumPt2 = nullptr; //!
		std::vector<float>* m_Lxy_Min_A0       = nullptr; //!
		std::vector<float>* m_LxyErr_Min_A0    = nullptr; //!
		std::vector<float>* m_Lxy_Min_Z0       = nullptr; //!
		std::vector<float>* m_LxyErr_Min_Z0    = nullptr; //!

		std::vector<int>* m_DiMuon_TriggMatch = nullptr; //!

		/*
		std::vector<int>*   m_DiMuon_IsMatch_2mu4_bUpsimumu = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu4_bJpsimumu = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu4_bUpsimumu_noL2 = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu4_bJpsimumu_noL2 = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu4_bDimu = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu4_bDimu_noinvm_novtx_ss = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu4_bDimu_novtx_noos = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_mu6_mu4_bUpsimumu = nullptr;   //!
		std::vector<int>*   m_DiMuon_IsMatch_mu6_mu4_bJpsimumu = nullptr;   //!
		std::vector<int>*   m_DiMuon_IsMatch_mu6_mu4_bUpsimumu_noL2 = nullptr;   //!
		std::vector<int>*   m_DiMuon_IsMatch_mu6_mu4_bJpsimumu_noL2 = nullptr;   //!
		std::vector<int>*   m_DiMuon_IsMatch_2mu6_bUpsimumu = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu6_bJpsimumu = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu6_bUpsimumu_noL2 = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu6_bJpsimumu_noL2 = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu10_bUpsimumu = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu10_bJpsimumu = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu10_bUpsimumu_noL2 = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_2mu10_bJpsimumu_noL2 = nullptr;//!
		std::vector<int>*   m_DiMuon_IsMatch_mu20_iloose_L1MU15 = nullptr;      //!
		std::vector<int>*   m_DiMuon_IsMatch_mu24_ivarmedium = nullptr;					//!
		std::vector<int>*   m_DiMuon_IsMatch_mu26_ivarmedium = nullptr;					//!
		std::vector<int>*   m_DiMuon_IsMatch_mu18_2mu0noL1_JpsimumuFS = nullptr;	//!
		std::vector<int>*   m_DiMuon_IsMatch_mu20_2mu0noL1_JpsimumuFS = nullptr; //!
		std::vector<int>*   m_DiMuon_IsMatch_mu20_2mu2noL1_JpsimumuFS = nullptr; //!
		*/

		std::vector<float>* m_Muon_Phi_hx         = nullptr;              //!
		std::vector<float>* m_Muon_CosTheta_hx    = nullptr;         //!
		std::vector<float>* m_Muon_Phi_hxOLD      = nullptr;              //!
		std::vector<float>* m_Muon_CosTheta_hxOLD = nullptr;         //!

		//std::vector<float>* m_Muon0_RecoEff = nullptr;            //!
		std::vector<int>*    m_Muon0_Charge = nullptr;             //!
		std::vector<int>*    m_Muon0_Type = nullptr;               //!
		std::vector<int>*    m_Muon0_PassedIDCuts = nullptr;       //!
		std::vector<int>*    m_Muon0_isCombined = nullptr;         //!
		std::vector<float>*  m_Muon0_Track_d0 = nullptr;           //!
		std::vector<float>*  m_Muon0_Track_z0 = nullptr;           //!
		std::vector<TLorentzVector>*  m_Muon0_RefTrack_p4 = nullptr;  //!
		std::vector<TLorentzVector>*  m_Muon0_Track_p4 = nullptr;  //!
		std::vector<TLorentzVector>*  m_Muon0_CBTrack_p4 = nullptr;//!
		//std::vector<float>*  m_Muon0_RefTrack_Pt = nullptr;        //!
		//std::vector<float>*  m_Muon0_RefTrack_Eta = nullptr;       //!
		//std::vector<float>*  m_Muon0_RefTrack_Phi = nullptr;       //!
		//std::vector<float>*  m_Muon0_RefTrack_Theta = nullptr;     //!
		//std::vector<float>*  m_Muon0_Track_Pt = nullptr;           //!
		//std::vector<float>*  m_Muon0_Track_Eta = nullptr;          //!
		//std::vector<float>*  m_Muon0_Track_Phi = nullptr;          //!
		//std::vector<float>*  m_Muon0_Track_Theta = nullptr;        //!
		//std::vector<float>*  m_Muon0_MeTrack_Pt = nullptr;         //!
		//std::vector<float>*  m_Muon0_MeTrack_Eta = nullptr;        //!
		//std::vector<float>*  m_Muon0_MeTrack_Phi = nullptr;        //!
		//std::vector<float>*  m_Muon0_MeTrack_Theta = nullptr;      //!
		//std::vector<float>*  m_Muon0_CBTrack_Pt = nullptr;         //!
		//std::vector<float>*  m_Muon0_CBTrack_Eta = nullptr;        //!
		//std::vector<float>*  m_Muon0_CBTrack_Phi = nullptr;        //!
		//std::vector<float>*  m_Muon0_CBTrack_Theta = nullptr;      //!
		std::vector<int>*    m_Muon0_Quality = nullptr;            //!
		std::vector<int>*    m_Muon0_TriggMatch = nullptr;         //!
		//std::vector<float>*  m_Muon0_qOverPsignif = nullptr;       //!
		//std::vector<float>*  m_Muon0_qOverPsigma = nullptr;        //!
		//std::vector<float>*  m_Muon0_MatchdR_mu4 = nullptr;        //!
		//std::vector<float>*  m_Muon0_MatchdR_mu6 = nullptr;        //!
		//std::vector<float>*  m_Muon0_MatchdR_mu4_trk = nullptr;    //!
		//std::vector<float>*  m_Muon0_MatchdR_mu6_trk = nullptr;    //!
		//std::vector<float>*  m_Muon0_MatchdR_mu20 = nullptr;       //!
		//std::vector<float>*  m_Muon0_MatchdR_mu20i = nullptr;      //!
		//std::vector<float>*  m_Muon0_MatchdR_MU4 = nullptr;        //!
		//std::vector<float>*  m_Muon0_MatchdR_MU6 = nullptr;        //!
		//std::vector<float>*  m_Muon0_MatchdR_MU15 = nullptr;       //!


		//std::vector<float>* m_Muon1_RecoEff = nullptr;            //!
		std::vector<int>*    m_Muon1_Charge = nullptr;             //!
		std::vector<int>*    m_Muon1_Type = nullptr;               //!
		std::vector<int>*    m_Muon1_PassedIDCuts = nullptr;       //!
		std::vector<int>*    m_Muon1_isCombined = nullptr;         //!
		std::vector<float>*  m_Muon1_Track_d0 = nullptr;           //!
		std::vector<float>*  m_Muon1_Track_z0 = nullptr;           //!
		std::vector<TLorentzVector>*  m_Muon1_RefTrack_p4 = nullptr;  //!
		std::vector<TLorentzVector>*  m_Muon1_Track_p4 = nullptr;  //!
		std::vector<TLorentzVector>*  m_Muon1_CBTrack_p4 = nullptr;//!
		//std::vector<float>*  m_Muon1_RefTrack_Pt = nullptr;        //!
		//std::vector<float>*  m_Muon1_RefTrack_Eta = nullptr;       //!
		//std::vector<float>*  m_Muon1_RefTrack_Phi = nullptr;       //!
		//std::vector<float>*  m_Muon1_RefTrack_Theta = nullptr;     //!
		//std::vector<float>*  m_Muon1_Track_Pt = nullptr;           //!
		//std::vector<float>*  m_Muon1_Track_Eta = nullptr;          //!
		//std::vector<float>*  m_Muon1_Track_Phi = nullptr;          //!
		//std::vector<float>*  m_Muon1_Track_Theta = nullptr;        //!
		//std::vector<float>*  m_Muon1_MeTrack_Pt = nullptr;         //!
		//std::vector<float>*  m_Muon1_MeTrack_Eta = nullptr;        //!
		//std::vector<float>*  m_Muon1_MeTrack_Phi = nullptr;        //!
		//std::vector<float>*  m_Muon1_MeTrack_Theta = nullptr;      //!
		//std::vector<float>*  m_Muon1_CBTrack_Pt = nullptr;         //!
		//std::vector<float>*  m_Muon1_CBTrack_Eta = nullptr;        //!
		//std::vector<float>*  m_Muon1_CBTrack_Phi = nullptr;        //!
		//std::vector<float>*  m_Muon1_CBTrack_Theta = nullptr;      //!
		std::vector<int>*    m_Muon1_Quality = nullptr;            //!
		std::vector<int>*    m_Muon1_TriggMatch = nullptr;         //!
		//std::vector<float>*  m_Muon1_qOverPsignif = nullptr;       //!
		//std::vector<float>*  m_Muon1_qOverPsigma = nullptr;        //!
		//std::vector<float>*  m_Muon1_MatchdR_mu4 = nullptr;        //!
		//std::vector<float>*  m_Muon1_MatchdR_mu6 = nullptr;        //!
		//std::vector<float>*  m_Muon1_MatchdR_mu4_trk = nullptr;    //!
		//std::vector<float>*  m_Muon1_MatchdR_mu6_trk = nullptr;    //!
		//std::vector<float>*  m_Muon1_MatchdR_mu20 = nullptr;       //!
		//std::vector<float>*  m_Muon1_MatchdR_mu20i = nullptr;      //!
		//std::vector<float>*  m_Muon1_MatchdR_MU4 = nullptr;        //!
		//std::vector<float>*  m_Muon1_MatchdR_MU6 = nullptr;        //!
		//std::vector<float>*  m_Muon1_MatchdR_MU15 = nullptr;       //!

		std::vector<float>*  m_Muon0_RecoEffTight  = nullptr;       //!
		std::vector<float>*  m_Muon1_RecoEffTight  = nullptr;       //!
		std::vector<float>*  m_Muon0_RecoEffMedium = nullptr;       //!
		std::vector<float>*  m_Muon1_RecoEffMedium = nullptr;       //!
		std::vector<float>*  m_Muon0_RecoEffLoose  = nullptr;       //!
		std::vector<float>*  m_Muon1_RecoEffLoose  = nullptr;       //!
		std::vector<float>*  m_Muon0_RecoEffLowPt  = nullptr;       //!
		std::vector<float>*  m_Muon1_RecoEffLowPt  = nullptr;       //!

		std::vector<float>*  m_Muon0_mu50TriggEffTight  = nullptr;     //!
		std::vector<float>*  m_Muon0_mu50TriggEffMedium = nullptr;     //!
		std::vector<float>*  m_Muon0_mu50TriggEffLoose  = nullptr;     //!
		std::vector<float>*  m_Muon1_mu50TriggEffTight  = nullptr;     //!
		std::vector<float>*  m_Muon1_mu50TriggEffMedium = nullptr;     //!
		std::vector<float>*  m_Muon1_mu50TriggEffLoose  = nullptr;     //!

		std::vector<TLorentzVector>* m_mu_DiMuon_p4 = nullptr; //!
		std::vector<int>* m_mu_DiMuon_TriggMatch = nullptr; //!
		std::vector<float>* m_mu_Muon_Phi_hx = nullptr; //!
		std::vector<float>* m_mu_Muon_CosTheta_hx = nullptr; //!
		std::vector<float>* m_mu_Muon_Phi_hxOLD = nullptr; //!
		std::vector<float>* m_mu_Muon_CosTheta_hxOLD = nullptr; //!
		std::vector<TLorentzVector>* m_mu_Muon0_CBTrack_p4 = nullptr; //!
		std::vector<int>* m_mu_Muon0_Charge = nullptr; //!
		std::vector<int>* m_mu_Muon0_Quality = nullptr; //!
		std::vector<int>* m_mu_Muon0_TriggMatch = nullptr; //!
		std::vector<TLorentzVector>* m_mu_Muon1_CBTrack_p4 = nullptr; //!
		std::vector<int>* m_mu_Muon1_Charge = nullptr; //!
		std::vector<int>* m_mu_Muon1_Quality = nullptr; //!
		std::vector<int>* m_mu_Muon1_TriggMatch = nullptr; //!
		std::vector<float>* m_mu_Muon0_RecoEffTight  = nullptr; //!
		std::vector<float>* m_mu_Muon1_RecoEffTight  = nullptr; //!
		std::vector<float>* m_mu_Muon0_RecoEffMedium = nullptr; //!
		std::vector<float>* m_mu_Muon1_RecoEffMedium = nullptr; //!
		std::vector<float>* m_mu_Muon0_RecoEffLoose   = nullptr; //!
		std::vector<float>* m_mu_Muon1_RecoEffLoose   = nullptr; //!
		std::vector<float>* m_mu_Muon0_RecoEffLowPt   = nullptr; //!
		std::vector<float>* m_mu_Muon1_RecoEffLowPt   = nullptr; //!
		std::vector<float>* m_mu_Muon0_mu50TriggEffTight   = nullptr; //!
		std::vector<float>* m_mu_Muon1_mu50TriggEffTight   = nullptr; //!
		std::vector<float>* m_mu_Muon0_mu50TriggEffMedium  = nullptr; //!
		std::vector<float>* m_mu_Muon1_mu50TriggEffMedium  = nullptr; //!
		std::vector<float>* m_mu_Muon0_mu50TriggEffLoose   = nullptr; //!
		std::vector<float>* m_mu_Muon1_mu50TriggEffLoose   = nullptr; //!

		//Example algorithm property, see constructor for declaration:
		//int m_nProperty = 0;

		//Example histogram, see initialize method for registration to output histSvc
		//TH1D* m_myHist = 0;
		//TTree* m_myTree = 0;

};

#endif //> !ONIAANA_ONIAANAALG_H
