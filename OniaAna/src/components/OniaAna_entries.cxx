
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../OniaAnaAlg.h"

DECLARE_ALGORITHM_FACTORY( OniaAnaAlg )

DECLARE_FACTORY_ENTRIES( OniaAna ) 
{
  DECLARE_ALGORITHM( OniaAnaAlg );
}
