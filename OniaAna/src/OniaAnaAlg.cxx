// OniaAna includes
#include "OniaAnaAlg.h"

//#include "xAODEventInfo/EventInfo.h"

#include "BLSToolBox/BLSToolBox.h"



OniaAnaAlg::OniaAnaAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ),
	m_toolbox("BLToolBox/ToolBox")
{

	m_toolbox.declarePropertyFor( this, "ToolBox" , "Configured toolbox" ); //example property declaration

	declareProperty( "DiMuonKey", m_bphysVertexKey = "BPHY1OniaCandidates", "Di-muons" );
	declareProperty( "Year", m_Year = "Data15");
	declareProperty( "MuonLoop", m_MuonLoop = false);
	//declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


OniaAnaAlg::~OniaAnaAlg() {}


StatusCode OniaAnaAlg::initialize() {
	ATH_MSG_INFO ("Initializing " << name() << "...");
	//
	//This is called once, before the start of the event loop
	//Retrieves of tools you have configured in the joboptions go here
	//



	//HERE IS AN EXAMPLE
	//We will create a histogram and a ttree and register them to the histsvc
	//Remember to configure the histsvc stream in the joboptions
	//
	//m_myHist = new TH1D("myHist","myHist",100,0,100);
	//CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
	m_myTree = new TTree("tree","myTree");
	CHECK( histSvc()->regTree("/MYSTREAM/tree", m_myTree) ); //registers tree to output stream inside a sub-directory
	ATH_CHECK(bookBranches());

	m_toolbox.setTypeAndName("BLS::BLSToolBox/BLSToolBox");
	ATH_CHECK( m_toolbox.retrieve() );
	ATH_MSG_INFO("Retrieved tool: " << m_toolbox->name() );

	//Trigger list
	//Format: pair<Name, wheter it's dimuon trigger or not>
	std::vector<std::pair<std::string,bool>> triggerNames15 = {
		std::make_pair("HLT_2mu4_bJpsimumu",1),            //0
		std::make_pair("HLT_2mu4_bJpsimumu_noL2",1),       //1
		std::make_pair("HLT_2mu6_bJpsimumu",1),            //2
		std::make_pair("HLT_2mu6_bJpsimumu_noL2",1),       //3
		std::make_pair("HLT_mu6_mu4_bJpsimumu",1),         //4
		std::make_pair("HLT_mu6_mu4_bJpsimumu_noL2",1),    //5
		std::make_pair("HLT_2mu10_bJpsimumu",1),           //6
		std::make_pair("HLT_2mu10_bJpsimumu_noL2",1),      //7
		std::make_pair("HLT_mu18_mu8noL1",1),              //8
		std::make_pair("HLT_mu20_mu8noL1",1),              //9
		std::make_pair("HLT_mu22_mu8noL1",1),              //10
		std::make_pair("HLT_mu24_mu8noL1",1),              //11
		std::make_pair("HLT_mu40",0),                      //12
		std::make_pair("HLT_mu50",0)                       //13
	};

	std::vector<std::pair<std::string,bool>> triggerNames16 = {
		std::make_pair("HLT_mu6_mu4_bJpsimumu",1),                //0
		std::make_pair("HLT_mu6_mu4_bJpsimumu_delayed",1),        //1
		std::make_pair("HLT_2mu6_bJpsimumu",1),                   //2
		std::make_pair("HLT_2mu6_bJpsimumu_delayed",1),           //3
		std::make_pair("HLT_mu10_mu6_bJpsimumu",1),               //4
		std::make_pair("HLT_mu10_mu6_bJpsimumu_delayed",1),       //5
		std::make_pair("HLT_2mu10_bJpsimumu",1),                  //6
		std::make_pair("HLT_2mu10_bJpsimumu_delayed",1),          //7
		std::make_pair("HLT_2mu10_bJpsimumu_noL2",1),             //8
		std::make_pair("HLT_mu20_nomucomb_mu6noL1_nscan03",1),    //9
		std::make_pair("HLT_mu22_mu8noL1",1),                     //10
		std::make_pair("HLT_mu24_mu8noL1",1),                     //11
		std::make_pair("HLT_mu26_mu8noL1",1),                     //12
		std::make_pair("HLT_mu50",0)                              //13
	};

	std::vector<std::pair<std::string,bool>> triggerNames17 = {
		std::make_pair("HLT_2mu10_bJpsimumu",1),                                //0
		std::make_pair("HLT_2mu10_bJpsimumu_noL2",1),                           //1
		std::make_pair("HLT_2mu6_bJpsimumu_L1BPH-2M9-2MU6_BPH-2DR15-2MU6",1),   //2
		std::make_pair("HLT_mu11_mu6_bJpsimumu",1),                             //3
		std::make_pair("HLT_mu22_mu8noL1",1),                                   //4
		std::make_pair("HLT_mu24_mu10noL1",1),                                  //5
		std::make_pair("HLT_mu24_mu8noL1",1),                                   //6
		std::make_pair("HLT_mu26_mu10noL1",1),                                  //7
		std::make_pair("HLT_mu26_mu8noL1",1),                                   //8
		std::make_pair("HLT_mu28_mu8noL1",1),                                   //9
		std::make_pair("HLT_mu50",0),                                           //10
		std::make_pair("HLT_mu60",0),                                           //11
		std::make_pair("HLT_mu80",0),                                           //12
		std::make_pair("HLT_2mu14",1)                                           //13
	};

	std::vector<std::pair<std::string,bool>> triggerNames18 = {
		std::make_pair("HLT_2mu10_bJpsimumu",1),                                //0
		std::make_pair("HLT_2mu10_bJpsimumu_noL2",1),                           //1
		std::make_pair("HLT_2mu6_bJpsimumu_L1BPH-2M9-2MU6_BPH-2DR15-2MU6",1),   //2
		std::make_pair("HLT_mu11_mu6_bJpsimumu",1),                             //3
		std::make_pair("HLT_mu50",0),                                           //4
		std::make_pair("HLT_mu60",0),                                           //5
		std::make_pair("HLT_mu80",0)                                            //6
	};

	if(m_Year == "Is"){
		m_triggerNames = triggerNames16; //For mc use triggerNames from data16
	}
	else{
		if(m_Year == "Data15")
			m_triggerNames = triggerNames15;
		else if(m_Year == "Data16")
			m_triggerNames = triggerNames16;
		else if(m_Year == "Data17")
			m_triggerNames = triggerNames17;
		else if(m_Year == "Data18")
			m_triggerNames = triggerNames18;
		else{
			ATH_MSG_ERROR("No configuration found for "<<m_Year);
			return StatusCode::SUCCESS;
		}
	}

	ATH_MSG_INFO("Year: "<<m_Year);
	ATH_MSG_INFO("DiMuon triggers:");
	for(unsigned int iTrg(0); iTrg<m_triggerNames.size(); iTrg++){
		if(m_triggerNames[iTrg].second)
			ATH_MSG_INFO(m_triggerNames[iTrg].first);
	}
	ATH_MSG_INFO("Single muon triggers:");
	for(unsigned int iTrg(0); iTrg<m_triggerNames.size(); iTrg++){
		if(!m_triggerNames[iTrg].second)
			ATH_MSG_INFO(m_triggerNames[iTrg].first);
	}

	//Preassign memory
	m_PS = new std::vector<float>();

	m_Truth_Parent_PdgId = new std::vector<float>();
	m_Truth_Parent_Pt = new std::vector<float>();
	m_Truth_Parent_Eta = new std::vector<float>();
	m_Truth_Parent_Phi = new std::vector<float>();
	m_Truth_Parent_Mass = new std::vector<float>();
	m_Truth_Onia_PdgId = new std::vector<float>();
	m_Truth_Onia_Pt = new std::vector<float>();
	m_Truth_Onia_Eta = new std::vector<float>();
	m_Truth_Onia_Phi = new std::vector<float>();
	m_Truth_Onia_Mass = new std::vector<float>();
	m_Truth_Onia_Vertex_X = new std::vector<float>();
	m_Truth_Onia_Vertex_Y = new std::vector<float>();
	m_Truth_Onia_Vertex_Z = new std::vector<float>();
	m_Truth_MuMinus_Pt = new std::vector<float>();
	m_Truth_MuMinus_Eta = new std::vector<float>();
	m_Truth_MuMinus_Phi = new std::vector<float>();
	m_Truth_MuPlus_Pt = new std::vector<float>();
	m_Truth_MuPlus_Eta = new std::vector<float>();
	m_Truth_MuPlus_Phi = new std::vector<float>();

	m_DiMuon_Mass = new std::vector<float>();
	m_DiMuon_Pt = new std::vector<float>();
	m_DiMuon_MassErr = new std::vector<float>();
	m_DiMuon_Rapidity = new std::vector<float>();
	m_DiMuon_ChiSq = new std::vector<float>();
	m_DiMuon_Vertex_X = new std::vector<float>();
	m_DiMuon_Vertex_Y = new std::vector<float>();
	m_DiMuon_Vertex_Z = new std::vector<float>();
	ATH_MSG_VERBOSE ("Line " << __LINE__);

	m_Tau_InvMass_MaxSumPt2      = new std::vector<float>();
	m_Tau_JpsiMass_MaxSumPt2     = new std::vector<float>();
	m_TauErr_InvMass_MaxSumPt2   = new std::vector<float>();
	m_TauErr_JpsiMass_MaxSumPt2  = new std::vector<float>();
	m_Tau_InvMass_Min_A0         = new std::vector<float>();
	m_Tau_JpsiMass_Min_A0        = new std::vector<float>();
	m_TauErr_InvMass_Min_A0      = new std::vector<float>();
	m_TauErr_JpsiMass_Min_A0     = new std::vector<float>();
	m_Tau_InvMass_Min_Z0         = new std::vector<float>();
	m_Tau_JpsiMass_Min_Z0        = new std::vector<float>();
	m_TauErr_InvMass_Min_Z0      = new std::vector<float>();
	m_TauErr_JpsiMass_Min_Z0     = new std::vector<float>();

	m_Lxy_MaxSumPt2    = new std::vector<float>();
	m_LxyErr_MaxSumPt2 = new std::vector<float>();
	m_Lxy_Min_A0       = new std::vector<float>();
	m_LxyErr_Min_A0    = new std::vector<float>();
	m_Lxy_Min_Z0       = new std::vector<float>();
	m_LxyErr_Min_Z0    = new std::vector<float>();

	ATH_MSG_VERBOSE ("Line " << __LINE__);

	m_DiMuon_TriggMatch = new std::vector<int>();
	ATH_MSG_VERBOSE ("Line " << __LINE__);

	m_Muon0_Charge = new std::vector<int>();
	m_Muon0_Track_d0 = new std::vector<float>();
	m_Muon0_Track_z0 = new std::vector<float>();
	m_Muon0_RefTrack_p4 = new std::vector<TLorentzVector>();
	m_Muon0_Track_p4 = new std::vector<TLorentzVector>();
	m_Muon0_CBTrack_p4 = new std::vector<TLorentzVector>();
	//m_Muon0_RefTrack_Pt = new std::vector<float>();
	//m_Muon0_RefTrack_Eta = new std::vector<float>();
	//m_Muon0_RefTrack_Phi = new std::vector<float>();
	//m_Muon0_RefTrack_Theta = new std::vector<float>();
	//m_Muon0_Track_Pt = new std::vector<float>();
	//m_Muon0_Track_Eta = new std::vector<float>();
	//m_Muon0_Track_Phi = new std::vector<float>();
	//m_Muon0_Track_Theta = new std::vector<float>();
	//m_Muon0_CBTrack_Pt = new std::vector<float>();
	//m_Muon0_CBTrack_Eta = new std::vector<float>();
	//m_Muon0_CBTrack_Phi = new std::vector<float>();
	m_Muon0_Quality = new std::vector<int>();
	m_Muon0_TriggMatch = new std::vector<int>();
	ATH_MSG_VERBOSE ("Line " << __LINE__);


	m_Muon1_Charge = new std::vector<int>();
	m_Muon_Phi_hx = new std::vector<float>();
	m_Muon_CosTheta_hx = new std::vector<float>();
	m_Muon_Phi_hxOLD = new std::vector<float>();
	m_Muon_CosTheta_hxOLD = new std::vector<float>();

	m_Muon1_Track_d0 = new std::vector<float>();
	m_Muon1_Track_z0 = new std::vector<float>();
	m_Muon1_RefTrack_p4 = new std::vector<TLorentzVector>();
	m_Muon1_Track_p4 = new std::vector<TLorentzVector>();
	m_Muon1_CBTrack_p4 = new std::vector<TLorentzVector>();
	//m_Muon1_RefTrack_Pt = new std::vector<float>();
	//m_Muon1_RefTrack_Eta = new std::vector<float>();
	//m_Muon1_RefTrack_Phi = new std::vector<float>();
	//m_Muon1_RefTrack_Theta = new std::vector<float>();
	//m_Muon1_Track_Pt = new std::vector<float>();
	//m_Muon1_Track_Eta = new std::vector<float>();
	//m_Muon1_Track_Phi = new std::vector<float>();
	//m_Muon1_Track_Theta = new std::vector<float>();
	//m_Muon1_CBTrack_Pt = new std::vector<float>();
	//m_Muon1_CBTrack_Eta = new std::vector<float>();
	//m_Muon1_CBTrack_Phi = new std::vector<float>();
	m_Muon1_Quality = new std::vector<int>();
	m_Muon1_TriggMatch = new std::vector<int>();

	m_Muon0_RecoEffTight  = new std::vector<float>();
	m_Muon1_RecoEffTight  = new std::vector<float>();
	m_Muon0_RecoEffMedium = new std::vector<float>();
	m_Muon1_RecoEffMedium = new std::vector<float>();
	m_Muon0_RecoEffLoose  = new std::vector<float>();
	m_Muon1_RecoEffLoose  = new std::vector<float>();
	m_Muon0_RecoEffLowPt  = new std::vector<float>();
	m_Muon1_RecoEffLowPt  = new std::vector<float>();

	m_Muon0_mu50TriggEffTight  = new std::vector<float>();
	m_Muon0_mu50TriggEffMedium = new std::vector<float>();
	m_Muon0_mu50TriggEffLoose  = new std::vector<float>();
	m_Muon1_mu50TriggEffTight  = new std::vector<float>();
	m_Muon1_mu50TriggEffMedium = new std::vector<float>();
	m_Muon1_mu50TriggEffLoose  = new std::vector<float>();

	//For "custom (bphy20)" dimuons
	m_mu_DiMuon_p4 = new std::vector<TLorentzVector>();
	m_mu_DiMuon_TriggMatch = new std::vector<int>();
	m_mu_Muon_Phi_hx = new std::vector<float>();
	m_mu_Muon_CosTheta_hx = new std::vector<float>();
	m_mu_Muon_Phi_hxOLD = new std::vector<float>();
	m_mu_Muon_CosTheta_hxOLD = new std::vector<float>();
	m_mu_Muon0_CBTrack_p4 = new std::vector<TLorentzVector>();
	m_mu_Muon0_Charge = new std::vector<int>();
	m_mu_Muon0_Quality = new std::vector<int>();
	m_mu_Muon0_TriggMatch = new std::vector<int>();
	m_mu_Muon1_CBTrack_p4 = new std::vector<TLorentzVector>();
	m_mu_Muon1_Charge = new std::vector<int>();
	m_mu_Muon1_Quality = new std::vector<int>();
	m_mu_Muon1_TriggMatch = new std::vector<int>();
	m_mu_Muon0_RecoEffTight  = new std::vector<float>();
	m_mu_Muon1_RecoEffTight  = new std::vector<float>();
	m_mu_Muon0_RecoEffMedium = new std::vector<float>();
	m_mu_Muon1_RecoEffMedium = new std::vector<float>();
	m_mu_Muon0_RecoEffLoose   = new std::vector<float>();
	m_mu_Muon1_RecoEffLoose   = new std::vector<float>();
	m_mu_Muon0_RecoEffLowPt   = new std::vector<float>();
	m_mu_Muon1_RecoEffLowPt   = new std::vector<float>();
	m_mu_Muon0_mu50TriggEffTight   = new std::vector<float>();
	m_mu_Muon1_mu50TriggEffTight   = new std::vector<float>();
	m_mu_Muon0_mu50TriggEffMedium  = new std::vector<float>();
	m_mu_Muon1_mu50TriggEffMedium  = new std::vector<float>();
	m_mu_Muon0_mu50TriggEffLoose   = new std::vector<float>();
	m_mu_Muon1_mu50TriggEffLoose   = new std::vector<float>();

	return StatusCode::SUCCESS;
}

StatusCode OniaAnaAlg::finalize() {
	ATH_MSG_INFO ("Finalizing " << name() << "...");
	//
	//Things that happen once at the end of the event loop go here
	//


	return StatusCode::SUCCESS;
}

StatusCode OniaAnaAlg::execute() {
	ATH_MSG_DEBUG ("Executing " << name() << "...");
	setFilterPassed(false); //optional: start with algorithm not passed
	const double m_MuonMass(105.6583745);

	ATH_CHECK(clearBranches());
	ATH_MSG_VERBOSE ("Line " << __LINE__);

	auto muons    = m_toolbox->muons  ();
	auto tracks   = m_toolbox->tracks ();
	auto pvs      = m_toolbox->pvs    ();

	const xAOD::TruthEventContainer*    truthEvents   (nullptr);
	const xAOD::TruthParticleContainer* truthParticles(nullptr);
	const xAOD::TruthVertexContainer*   truthVertices (nullptr);
	ATH_MSG_VERBOSE ("Line " << __LINE__);

	auto DiMuonVertices  = m_toolbox->vertices(m_bphysVertexKey);
	ATH_MSG_VERBOSE ("Line " << __LINE__);

	bool isMC = m_toolbox->isMC();
	if (isMC) {
		truthEvents    = m_toolbox->truthEvents   ();
		truthParticles = m_toolbox->truthParticles();
		truthVertices  = m_toolbox->truthVertices ();
	}


	auto eventInfo    = m_toolbox->eventInfo();
	m_EventNumber = m_toolbox->eventNumber();
	m_RunNumber   = m_toolbox->runNumber();
	m_Pileup = m_toolbox->averageInteractionsPerCrossing();
	m_LumiBlock   = m_toolbox->lumiBlock();
	m_mcChannelNumber = -999;

	for(unsigned int iTrg(0); iTrg<m_triggerNames.size(); iTrg++)
		m_EventTrigger |= (m_toolbox->isPassed(m_triggerNames[iTrg].first) ? (int)pow(2,iTrg) : 0);

	auto trigDecTool = m_toolbox->tdt();
	for(unsigned int iTrg(0); iTrg<m_triggerNames.size(); iTrg++){
		float PS = trigDecTool->getChainGroup(m_triggerNames[iTrg].first)->getPrescale();
		m_PS->push_back(PS);
	}
	ATH_MSG_VERBOSE ("Line " << __LINE__);


	//---------------------------
	// Truth Block
	//---------------------------
	bool writeMC = false;
	std::vector<int> OniaPDG;
	OniaPDG.clear();
	OniaPDG.push_back(443);
	OniaPDG.push_back(100443);
	OniaPDG.push_back(999443);
	OniaPDG.push_back(553);
	OniaPDG.push_back(100553);
	OniaPDG.push_back(200553);
	if (isMC) {
		m_mcChannelNumber = eventInfo->mcChannelNumber();
		const xAOD::TruthVertex* PriVertex = (*truthVertices)[0];
		m_Truth_PV_Z = PriVertex->z();
		m_Truth_PV_X = PriVertex->x();
		m_Truth_PV_Y = PriVertex->y();

		for (auto particle: *truthParticles) {

			bool FoundOnia = false;
			for (unsigned int p = 0; p < OniaPDG.size(); ++p) {
				if (particle->pdgId() == OniaPDG[p]) FoundOnia = true;
			}
			if (!FoundOnia) continue;
			//if(particle->nParents() < 1) continue;
			if(particle->nChildren() < 2) continue;
			const xAOD::TruthParticle* TruthMuon0 = particle->child(0);
			const xAOD::TruthParticle* TruthMuon1 = particle->child(1);
			if(TruthMuon0->absPdgId() != 13) continue;
			if(TruthMuon1->absPdgId() != 13) continue;
			if(!particle->hasDecayVtx()) continue;
			if(!particle->hasProdVtx()) continue;
			if(TruthMuon0->status() != 1) continue;
			if(TruthMuon1->status() != 1) continue;

			writeMC = true;

			m_Truth_Onia_PdgId->push_back( particle->pdgId());
			m_Truth_Onia_Pt->push_back( particle->p4().Pt());
			m_Truth_Onia_Eta->push_back( particle->p4().Eta());
			m_Truth_Onia_Phi->push_back( particle->p4().Phi());
			m_Truth_Onia_Mass->push_back( particle->p4().M());

			//const xAOD::TruthParticle *Parent = particle->parent();
			//m_Truth_Parent_PdgId->push_back( Parent->pdgId());
			//m_Truth_Parent_Pt->push_back( Parent->p4().Pt());
			//m_Truth_Parent_Eta->push_back( Parent->p4().Eta());
			//m_Truth_Parent_Phi->push_back( Parent->p4().Phi());
			//m_Truth_Parent_Mass->push_back( Parent->p4().M());

			//const xAOD::TruthVertex* prodVtx = particle->prodVtx();
			const xAOD::TruthVertex* decayVtx = particle->decayVtx();
			//std::cout<<prodVtx->x()<<" "<<prodVtx->y()-decayVtx->y()<<" "<<prodVtx->z()-decayVtx->z()<<std::endl;
			m_Truth_Onia_Vertex_X->push_back( decayVtx->x());
			m_Truth_Onia_Vertex_Y->push_back( decayVtx->y());
			m_Truth_Onia_Vertex_Z->push_back( decayVtx->z());

			if (TruthMuon0->pdgId() == 13 && TruthMuon1->pdgId() == -13) {
				m_Truth_MuPlus_Pt->push_back(TruthMuon1->p4().Pt());
				m_Truth_MuPlus_Eta->push_back(TruthMuon1->p4().Eta());
				m_Truth_MuPlus_Phi->push_back(TruthMuon1->p4().Phi());
				m_Truth_MuMinus_Pt->push_back(TruthMuon0->p4().Pt());
				m_Truth_MuMinus_Eta->push_back(TruthMuon0->p4().Eta());
				m_Truth_MuMinus_Phi->push_back(TruthMuon0->p4().Phi());
			} else if (TruthMuon0->pdgId() == -13 && TruthMuon1->pdgId() == 13) {
				m_Truth_MuPlus_Pt->push_back(TruthMuon0->p4().Pt());
				m_Truth_MuPlus_Eta->push_back(TruthMuon0->p4().Eta());
				m_Truth_MuPlus_Phi->push_back(TruthMuon0->p4().Phi());
				m_Truth_MuMinus_Pt->push_back(TruthMuon1->p4().Pt());
				m_Truth_MuMinus_Eta->push_back(TruthMuon1->p4().Eta());
				m_Truth_MuMinus_Phi->push_back(TruthMuon1->p4().Phi());
			} else {
				m_Truth_MuPlus_Pt->push_back(-999);
				m_Truth_MuPlus_Eta->push_back(-999);
				m_Truth_MuPlus_Phi->push_back(-999);
				m_Truth_MuMinus_Pt->push_back(-999);
				m_Truth_MuMinus_Eta->push_back(-999);
				m_Truth_MuMinus_Phi->push_back(-999);
			}
		}
	}
	ATH_MSG_VERBOSE ("Line " << __LINE__);

	ANA_CHECK(m_toolbox->prwTool()->apply(*eventInfo,false));


	//---------------------------
	// OniaCandidates Block
	//---------------------------
	//const xAOD::VertexContainer* DiMuonVertices = 0;
	//ANA_CHECK( event->retrieve( DiMuonVertices,"BPHY1OniaCandidates" ) );
	xAOD::VertexContainer::const_iterator dimu_itr = DiMuonVertices->begin();
	xAOD::VertexContainer::const_iterator dimu_end = DiMuonVertices->end();

	//    xAOD::Muon* Muon0 = NULL;
	//    xAOD::Muon* Muon1 = NULL;

	for ( ; dimu_itr != dimu_end; ++dimu_itr ) {
		const xAOD::Vertex * DiMuon = (*dimu_itr);
		bool isUpsiCandidate = DiMuon->auxdata<char>("passed_Upsi");
		bool isJpsiCandidate = DiMuon->auxdata<char>("passed_Jpsi");
		bool isPsiCandidate  = DiMuon->auxdata<char>("passed_Psi");
		if (!isUpsiCandidate && !isJpsiCandidate && !isPsiCandidate) continue;

		xAOD::BPhysHelper bVtx(DiMuon);

		// Links to Muons
		std::vector< ElementLink<xAOD::MuonContainer> > MuonLinks = DiMuon->auxdata< std::vector< ElementLink<xAOD::MuonContainer> > >("MuonLinks");
		const ElementLink< xAOD::MuonContainer >& MuonLink0 = MuonLinks[0];
		const ElementLink< xAOD::MuonContainer >& MuonLink1 = MuonLinks[1];
		const xAOD::Muon* Mu0 = NULL;
		const xAOD::Muon* Mu1 = NULL;
		if (MuonLink0.isValid()) Mu0 = *MuonLink0;
		if (MuonLink1.isValid()) Mu1 = *MuonLink1;
		if (Mu0 == NULL || Mu1 == NULL) continue;

		auto Muon0_ptr = m_toolbox->makeCalibMuon(Mu0);
		auto Muon1_ptr = m_toolbox->makeCalibMuon(Mu1);

		//        if (!m_muonSmear->correctedCopy(*Mu0, Muon0) || !m_muonSmear->correctedCopy(*Mu1, Muon1) ) {
		//            if (Muon0) { delete Muon0; Muon0 = NULL; }
		//            if (Muon1) { delete Muon1; Muon1 = NULL; }
		//            continue;
		//        }

		//        if (!m_muonSelection->accept(*Muon0) || !m_muonSelection->accept(*Muon1)) {
		//            if (Muon0) { delete Muon0; Muon0 = NULL; }
		//            if (Muon1) { delete Muon1; Muon1 = NULL; }
		//            continue;
		//        }

		// set the quality of the muons:
		// logic is Tight=0, Medium=1, Loose=2, LowPT=5;
		// stored as 1*isTight + 10*isMedium + 100*isLoose + 1000*LowPt
		int muon0_qual(0);
		int muon1_qual(0);
		bool muon0_loose(false), muon0_medium(false),muon0_tight(false), muon0_lowpt(false);
		bool muon1_loose(false), muon1_medium(false),muon1_tight(false), muon1_lowpt(false);
		bool bvar(false);
		if (m_toolbox->passMuonTight (Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 1;   muon0_tight =true;}
		if (m_toolbox->passMuonMedium(Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 10;  muon0_medium=true;}
		if (m_toolbox->passMuonLoose (Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 100; muon0_loose =true;}
		if (m_toolbox->passMuonLowPt (Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 1000;muon0_lowpt =true;}

		if (m_toolbox->passMuonTight (Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 1;   muon1_tight =true;}
		if (m_toolbox->passMuonMedium(Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 10;  muon1_medium=true;}
		if (m_toolbox->passMuonLoose (Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 100; muon1_loose =true;}
		if (m_toolbox->passMuonLowPt (Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 1000;muon1_lowpt =true;}

		//if ( (!muon0_medium || !muon1_medium) && (!muon0_lowpt || !muon1_lowpt) ) continue; //exclude loose muons
		// un comment this section; assume that selection of muon quality, will be done in ntuple
		//        if (!m_toolbox->passMuonTight(Muon0_ptr.get(),bvar) || !bvar) continue;
		//        if (!m_toolbox->passMuonTight(Muon1_ptr.get(),bvar) || !bvar) continue;


		const xAOD::TrackParticle * Track0 = NULL;
		const xAOD::TrackParticle * Track1 = NULL;
		const ElementLink< xAOD::TrackParticleContainer >& Link0 = Muon0_ptr->inDetTrackParticleLink();
		const ElementLink< xAOD::TrackParticleContainer >& Link1 = Muon1_ptr->inDetTrackParticleLink();
		if (Link0.isValid()) Track0 = *Link0;
		if (Link1.isValid()) Track1 = *Link1;
		if (Track0 == NULL || Track1 == NULL) {
			//            if (Muon0) { delete Muon0; Muon0 = NULL; }
			//            if (Muon1) { delete Muon1; Muon1 = NULL; }
			continue;
		}

		// JW: It is possible that the mouns and the tracks are not in the same sequence.
		// add sanity checks to ensure we get consistent objects
		auto bphys_muon0        = bVtx.muon(0);
		auto bphys_muon1        = bVtx.muon(1);
		auto bphys_muon0_trk    = *bphys_muon0->inDetTrackParticleLink();
		auto bphys_muon1_trk    = *bphys_muon1->inDetTrackParticleLink();
		auto bphys_refTrkOrig0  = bVtx.refTrkOrigin(0);
		auto bphys_refTrkOrig1  = bVtx.refTrkOrigin(1);

		ATH_MSG_VERBOSE ("      Muon0: " << Mu0);
		ATH_MSG_VERBOSE ("      Muon1: " << Mu1);
		ATH_MSG_VERBOSE ("      Trk0 : " << Track0);
		ATH_MSG_VERBOSE ("      Trk1 : " << Track1);

		ATH_MSG_VERBOSE ("BPhys Muon0: " << bphys_muon0);
		ATH_MSG_VERBOSE ("BPhys Muon1: " << bphys_muon1);
		ATH_MSG_VERBOSE ("BPhys Trk0 : " << bphys_muon0_trk);
		ATH_MSG_VERBOSE ("BPhys Trk1 : " << bphys_muon1_trk);
		ATH_MSG_VERBOSE ("BPhys OTrk0: " << bphys_refTrkOrig0);
		ATH_MSG_VERBOSE ("BPhys OTrk1: " << bphys_refTrkOrig1);

		ATH_MSG_VERBOSE ("      Trk0  Q: " << Track0->charge());
		ATH_MSG_VERBOSE ("      Trk1  Q: " << Track1->charge());
		ATH_MSG_VERBOSE ("BPhys OTrk0 Q: " << bVtx.refTrkCharge(0));
		ATH_MSG_VERBOSE ("BPhys OTrk1 Q: " << bVtx.refTrkCharge(1));

		ATH_MSG_VERBOSE ("------------------------------");

		if ( (Mu0 != bphys_muon0) || (Track0  != bphys_refTrkOrig0)) {
			ATH_MSG_WARNING("MISMATCH between original and BphysHelper muons / tracks for Mu0");
		}
		if ( (Mu0 != bphys_muon1) || (Track0  != bphys_refTrkOrig1)) {
			ATH_MSG_WARNING("MISMATCH between original and BphysHelper muons / tracks for Mu1");
		}

		TLorentzVector Muon0_RefTrack = bVtx.refTrk(0,m_MuonMass);
		TLorentzVector Muon1_RefTrack = bVtx.refTrk(1,m_MuonMass);
		TLorentzVector RefDiMuon = Muon0_RefTrack + Muon1_RefTrack;

		float DiMuon_Mass = 0;
		float DiMuon_MassErr = 0;
		if (isUpsiCandidate) {
			DiMuon_Mass    = DiMuon->auxdata<float>("Upsi_mass");
			DiMuon_MassErr = DiMuon->auxdata<float>("Upsi_massErr");
		} else if (isJpsiCandidate) {
			DiMuon_Mass    = DiMuon->auxdata<float>("Jpsi_mass");
			DiMuon_MassErr = DiMuon->auxdata<float>("Jpsi_massErr");
		} else if (isPsiCandidate) {
			DiMuon_Mass    = DiMuon->auxdata<float>("Psi_mass");
			DiMuon_MassErr = DiMuon->auxdata<float>("Psi_massErr");
		}
		m_DiMuon_Mass->push_back(DiMuon_Mass);
		m_DiMuon_MassErr->push_back(DiMuon_MassErr);
		m_DiMuon_Pt->push_back(RefDiMuon.Pt());
		m_DiMuon_Rapidity->push_back(RefDiMuon.Rapidity());
		m_DiMuon_ChiSq->push_back(DiMuon->chiSquared());
		m_DiMuon_Vertex_X->push_back(DiMuon->x());
		m_DiMuon_Vertex_Y->push_back(DiMuon->y());
		m_DiMuon_Vertex_Z->push_back(DiMuon->z());

		if (isJpsiCandidate) {
			m_Tau_JpsiMass_MaxSumPt2    ->push_back( DiMuon->auxdata<float>("Jpsi_TauConstMassPVMaxSumPt2")    );
			m_TauErr_JpsiMass_MaxSumPt2 ->push_back( DiMuon->auxdata<float>("Jpsi_TauErrConstMassPVMaxSumPt2") );
			m_Tau_InvMass_MaxSumPt2     ->push_back( DiMuon->auxdata<float>("Jpsi_TauInvMassPVMaxSumPt2")      );
			m_TauErr_InvMass_MaxSumPt2  ->push_back( DiMuon->auxdata<float>("Jpsi_TauErrInvMassPVMaxSumPt2")   );
			m_Tau_InvMass_Min_A0        ->push_back( DiMuon->auxdata<float>("Jpsi_TauInvMassPVMinA0")          );
			m_Tau_JpsiMass_Min_A0       ->push_back( DiMuon->auxdata<float>("Jpsi_TauConstMassPVMinA0")        );
			m_TauErr_InvMass_Min_A0     ->push_back( DiMuon->auxdata<float>("Jpsi_TauErrInvMassPVMinA0")       );
			m_TauErr_JpsiMass_Min_A0    ->push_back( DiMuon->auxdata<float>("Jpsi_TauErrConstMassPVMinA0")     );
			m_Tau_InvMass_Min_Z0        ->push_back( DiMuon->auxdata<float>("Jpsi_TauInvMassPVMinZ0")          );
			m_Tau_JpsiMass_Min_Z0       ->push_back( DiMuon->auxdata<float>("Jpsi_TauConstMassPVMinZ0")        );
			m_TauErr_InvMass_Min_Z0     ->push_back( DiMuon->auxdata<float>("Jpsi_TauErrInvMassPVMinZ0")       );
			m_TauErr_JpsiMass_Min_Z0    ->push_back( DiMuon->auxdata<float>("Jpsi_TauErrConstMassPVMinZ0")     );
		} else if (isUpsiCandidate) {
			m_Tau_JpsiMass_MaxSumPt2    ->push_back( DiMuon->auxdata<float>("Upsi_TauConstMassPVMaxSumPt2")    );
			m_TauErr_JpsiMass_MaxSumPt2 ->push_back( DiMuon->auxdata<float>("Upsi_TauErrConstMassPVMaxSumPt2") );
			m_Tau_InvMass_MaxSumPt2     ->push_back( DiMuon->auxdata<float>("Upsi_TauInvMassPVMaxSumPt2")      );
			m_TauErr_InvMass_MaxSumPt2  ->push_back( DiMuon->auxdata<float>("Upsi_TauErrInvMassPVMaxSumPt2")   );
			m_Tau_InvMass_Min_A0        ->push_back( DiMuon->auxdata<float>("Upsi_TauInvMassPVMinA0")          );
			m_Tau_JpsiMass_Min_A0       ->push_back( DiMuon->auxdata<float>("Upsi_TauConstMassPVMinA0")        );
			m_TauErr_InvMass_Min_A0     ->push_back( DiMuon->auxdata<float>("Upsi_TauErrInvMassPVMinA0")       );
			m_TauErr_JpsiMass_Min_A0    ->push_back( DiMuon->auxdata<float>("Upsi_TauErrConstMassPVMinA0")     );
			m_Tau_InvMass_Min_Z0        ->push_back( DiMuon->auxdata<float>("Upsi_TauInvMassPVMinZ0")          );
			m_Tau_JpsiMass_Min_Z0       ->push_back( DiMuon->auxdata<float>("Upsi_TauConstMassPVMinZ0")        );
			m_TauErr_InvMass_Min_Z0     ->push_back( DiMuon->auxdata<float>("Upsi_TauErrInvMassPVMinZ0")       );
			m_TauErr_JpsiMass_Min_Z0    ->push_back( DiMuon->auxdata<float>("Upsi_TauErrConstMassPVMinZ0")     );
		} else if (isPsiCandidate) {
			m_Tau_JpsiMass_MaxSumPt2    ->push_back( DiMuon->auxdata<float>("Psi_TauConstMassPVMaxSumPt2")    );
			m_TauErr_JpsiMass_MaxSumPt2 ->push_back( DiMuon->auxdata<float>("Psi_TauErrConstMassPVMaxSumPt2") );
			m_Tau_InvMass_MaxSumPt2     ->push_back( DiMuon->auxdata<float>("Psi_TauInvMassPVMaxSumPt2")      );
			m_TauErr_InvMass_MaxSumPt2  ->push_back( DiMuon->auxdata<float>("Psi_TauErrInvMassPVMaxSumPt2")   );
			m_Tau_InvMass_Min_A0        ->push_back( DiMuon->auxdata<float>("Psi_TauInvMassPVMinA0")          );
			m_Tau_JpsiMass_Min_A0       ->push_back( DiMuon->auxdata<float>("Psi_TauConstMassPVMinA0")        );
			m_TauErr_InvMass_Min_A0     ->push_back( DiMuon->auxdata<float>("Psi_TauErrInvMassPVMinA0")       );
			m_TauErr_JpsiMass_Min_A0    ->push_back( DiMuon->auxdata<float>("Psi_TauErrConstMassPVMinA0")     );
			m_Tau_InvMass_Min_Z0        ->push_back( DiMuon->auxdata<float>("Psi_TauInvMassPVMinZ0")          );
			m_Tau_JpsiMass_Min_Z0       ->push_back( DiMuon->auxdata<float>("Psi_TauConstMassPVMinZ0")        );
			m_TauErr_InvMass_Min_Z0     ->push_back( DiMuon->auxdata<float>("Psi_TauErrInvMassPVMinZ0")       );
			m_TauErr_JpsiMass_Min_Z0    ->push_back( DiMuon->auxdata<float>("Psi_TauErrConstMassPVMinZ0")     );
		}

		m_Lxy_MaxSumPt2    ->push_back( DiMuon ->auxdata<float>("LxyMaxSumPt2")    );
		m_LxyErr_MaxSumPt2 ->push_back( DiMuon ->auxdata<float>("LxyErrMaxSumPt2") );
		m_Lxy_Min_A0       ->push_back( DiMuon ->auxdata<float>("LxyMinA0")        );
		m_LxyErr_Min_A0    ->push_back( DiMuon ->auxdata<float>("LxyErrMinA0")     );
		m_Lxy_Min_Z0       ->push_back( DiMuon ->auxdata<float>("LxyMinZ0")        );
		m_LxyErr_Min_Z0    ->push_back( DiMuon ->auxdata<float>("LxyErrMinZ0")     );

		//        BphysMatchResult bphys_matching_bJpsimumu = m_trigMatching->match_to_bphysics_dimuon(*Muon0, *Muon1, "HLT_2mu4_bJpsimumu", 0.01);
		//        BphysMatchResult bphys_matching_bUpsimumu = m_trigMatching->match_to_bphysics_dimuon(*Muon0, *Muon1, "HLT_2mu4_bUpsimumu", 0.01);
		//        BphysMatchResult bphys_matching_bDimu     = m_trigMatching->match_to_bphysics_dimuon(*Muon0, *Muon1, "HLT_2mu4_bDimu", 0.01);
		//        BphysMatchResult bphys_matching_bDimu_ss  = m_trigMatching->match_to_bphysics_dimuon(*Muon0, *Muon1, "HLT_2mu4_bDimu_noinvm_novtx_ss", 0.01);
		//        BphysMatchResult bphys_matching_bDimu_noos= m_trigMatching->match_to_bphysics_dimuon(*Muon0, *Muon1, "HLT_2mu4_bDimu_novtx_noos", 0.01);
		//        BphysMatchResult bphys_matching_mu6_mu4_bJpsimumu = m_trigMatching->match_to_bphysics_dimuon(*Muon0, *Muon1, "HLT_mu6_mu4_bJpsimumu", 0.01);
		//        BphysMatchResult bphys_matching_mu6_mu4_bUpsimumu = m_trigMatching->match_to_bphysics_dimuon(*Muon0, *Muon1, "HLT_mu6_mu4_bUpsimumu", 0.01);
		//        BphysMatchResult bphys_matching_2mu6_bJpsimumu = m_trigMatching->match_to_bphysics_dimuon(*Muon0, *Muon1, "HLT_2mu6_bJpsimumu", 0.01);
		//        BphysMatchResult bphys_matching_2mu6_bUpsimumu = m_trigMatching->match_to_bphysics_dimuon(*Muon0, *Muon1, "HLT_2mu6_bUpsimumu", 0.01);
		//


		const std::vector<const xAOD::IParticle*> vecIptls = {Muon0_ptr.get(),Muon1_ptr.get()};
		const double matchThreshold(0.01);
		/*
		auto cg = trigDecTool->getChainGroup("HLT_mu18_mu8noL1");
		auto fc = cg->features();
		auto eleFeatureContainers = fc.containerFeature<xAOD::MuonContainer>();
		ATH_MSG_INFO("Looking at features for the event: ");
		for(auto &econt : eleFeatureContainers) {
			ATH_MSG_INFO("  -> TrigMuonContainer: " << econt.label());
			for (auto e : *econt.cptr()) {
				ATH_MSG_INFO("    -> mu pt = " << e->pt()/1000.0 << " [GeV]" );
			}
		}
		*/
		int triggMatch = 0; //Dimuon matches trigger
		int mu0_triggMatch = 0; //Mu1 matches trigger
		int mu1_triggMatch = 0; //Mu2 matches trigger
		for(unsigned int iTrg(0); iTrg<m_triggerNames.size(); iTrg++){
			if(m_triggerNames[iTrg].second){    //Dimuon trigger
				triggMatch |= (m_toolbox->match(vecIptls, m_triggerNames[iTrg].first, matchThreshold, false) ? (int)pow(2,iTrg) : 0);
				//If matched, then obviously both muons match
				if( triggMatch & (int)pow(2,iTrg)){
					mu0_triggMatch |= (int)pow(2,iTrg);
					mu1_triggMatch |= (int)pow(2,iTrg);
				}
			}
			else{                                 //Single mu trigger
				bool mu0_match(0), mu1_match(0);
				mu0_match = (m_toolbox->match(*vecIptls[0], m_triggerNames[iTrg].first, matchThreshold, false) ? 1 : 0);
				mu1_match = (m_toolbox->match(*vecIptls[1], m_triggerNames[iTrg].first, matchThreshold, false) ? 1 : 0);
				triggMatch |= ((mu0_match || mu1_match) ? (int)pow(2,iTrg) : 0);
				if(mu0_match){
					mu0_triggMatch |= (int)pow(2,iTrg);
				}
				if(mu1_match){
					mu1_triggMatch |= (int)pow(2,iTrg);
				}
			}
		}
		m_DiMuon_TriggMatch->push_back(triggMatch);
		m_Muon0_TriggMatch->push_back(mu0_triggMatch);
		m_Muon1_TriggMatch->push_back(mu1_triggMatch);


		// requires that muons have pass the Tight selection (as should be applied above).

		float _recoEffMuon0 = 1;
		float _recoEffMuon1 = 1;

		double _triggEffMuon0 = 1;
		double _triggEffMuon1 = 1;

		// Muon 0
		_recoEffMuon0 = -1.;
		_triggEffMuon0 = -1.;
		if (muon0_tight) {
			auto muonEffSFTool = m_toolbox->muonEffSFTightTool();
			//auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
			auto corrCodeMu0Eff = m_toolbox->getMCPEfficiency(muonEffSFTool, *Muon0_ptr, _recoEffMuon0, eventInfo, isMC);
			if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu0Eff Tight " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " <<Muon0_ptr->phi());
			}

			auto muonTriggSFTool = m_toolbox->muonTriggSFTightTool();
			auto corrCodeMu0Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon0_ptr, _triggEffMuon0, "HLT_mu50", !isMC);
			if (corrCodeMu0Trigg != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu0Trigg Tight " << corrCodeMu0Trigg << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " <<Muon0_ptr->phi());
			}
		}
		m_Muon0_RecoEffTight->push_back( _recoEffMuon0);
		m_Muon0_mu50TriggEffTight->push_back( _triggEffMuon0);

		_recoEffMuon0 = -1.;
		_triggEffMuon0 = -1.;
		if (muon0_medium) {
			auto muonEffSFTool = m_toolbox->muonEffSFMediumTool();
			auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
			if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu0Eff Medium " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " << Muon0_ptr->phi());

			}

			auto muonTriggSFTool = m_toolbox->muonTriggSFMediumTool();
			auto corrCodeMu0Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon0_ptr, _triggEffMuon0, "HLT_mu50", !isMC);
			if (corrCodeMu0Trigg != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu0Trigg Medium " << corrCodeMu0Trigg << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " <<Muon0_ptr->phi());
			}
		}
		m_Muon0_RecoEffMedium->push_back( _recoEffMuon0);
		m_Muon0_mu50TriggEffMedium->push_back( _triggEffMuon0);

		_recoEffMuon0 = -1.;
		_triggEffMuon0 = -1.;
		if (muon0_loose) {
			auto muonEffSFTool = m_toolbox->muonEffSFLooseTool();
			auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
			if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu0Eff Loose " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " << Muon0_ptr->phi());
			}

			auto muonTriggSFTool = m_toolbox->muonTriggSFLooseTool();
			auto corrCodeMu0Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon0_ptr, _triggEffMuon0, "HLT_mu50", !isMC);
			if (corrCodeMu0Trigg != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu0Trigg Loose " << corrCodeMu0Trigg << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " <<Muon0_ptr->phi());
			}
		}
		m_Muon0_RecoEffLoose->push_back( _recoEffMuon0);
		m_Muon0_mu50TriggEffLoose->push_back( _triggEffMuon0);

		_recoEffMuon0 = -1.;
		if (muon0_lowpt && m_toolbox->muonEffSFLowPtTool()) {
			auto muonEffSFTool = m_toolbox->muonEffSFLowPtTool();
			auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
			if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu0Eff LowPt " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " << Muon0_ptr->phi());
			}
		}
		m_Muon0_RecoEffLowPt->push_back( _recoEffMuon0);

		// Muon 1
		_recoEffMuon1 = -1.;
		_triggEffMuon1 = -1.;
		if (muon1_tight) {
			auto muonEffSFTool = m_toolbox->muonEffSFTightTool();
			auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
			if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu1Eff Tight " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
			}

			auto muonTriggSFTool = m_toolbox->muonTriggSFTightTool();
			auto corrCodeMu1Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon1_ptr, _triggEffMuon1, "HLT_mu50", !isMC);
			if (corrCodeMu1Trigg != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu1Trigg Tight " << corrCodeMu1Trigg << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " <<Muon1_ptr->phi());
			}
		}
		m_Muon1_RecoEffTight->push_back( _recoEffMuon1);
		m_Muon1_mu50TriggEffTight->push_back( _triggEffMuon1);

		_recoEffMuon1 = -1.;
		_triggEffMuon1 = -1.;
		if (muon1_medium) {
			auto muonEffSFTool = m_toolbox->muonEffSFMediumTool();
			auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
			if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu1Eff Medium " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
			}

			auto muonTriggSFTool = m_toolbox->muonTriggSFMediumTool();
			auto corrCodeMu1Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon1_ptr, _triggEffMuon1, "HLT_mu50", !isMC);
			if (corrCodeMu1Trigg != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu1Trigg Medium " << corrCodeMu1Trigg << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " <<Muon1_ptr->phi());
			}
		}
		m_Muon1_RecoEffMedium->push_back( _recoEffMuon1);
		m_Muon1_mu50TriggEffMedium->push_back( _triggEffMuon1);

		_recoEffMuon1 = -1.;
		_triggEffMuon1 = -1.;
		if (muon1_loose) {
			auto muonEffSFTool = m_toolbox->muonEffSFLooseTool();
			auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
			if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu1Eff Loose " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
			}

			auto muonTriggSFTool = m_toolbox->muonTriggSFLooseTool();
			auto corrCodeMu1Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon1_ptr, _triggEffMuon1, "HLT_mu50", !isMC);
			if (corrCodeMu1Trigg != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu1Trigg Loose " << corrCodeMu1Trigg << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " <<Muon1_ptr->phi());
			}
		}
		m_Muon1_RecoEffLoose->push_back( _recoEffMuon1);
		m_Muon1_mu50TriggEffLoose->push_back( _triggEffMuon1);

		_recoEffMuon1 = -1.;
		if (muon1_lowpt && m_toolbox->muonEffSFLowPtTool()) {
			auto muonEffSFTool = m_toolbox->muonEffSFLowPtTool();
			auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
			if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
				ATH_MSG_DEBUG ("corrCodeMu1Eff LowPt " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
			}
		}
		m_Muon1_RecoEffLowPt->push_back( _recoEffMuon1);




		double phi_hxOLD      = m_toolbox->phiMethod(Muon0_RefTrack,Muon1_RefTrack,Muon0_ptr->charge());
		double cosTheta_hxOLD = m_toolbox->cosMethod(Muon0_RefTrack,Muon1_RefTrack,Muon0_ptr->charge());

		//        m_muonEfficiency->getDataEfficiency(*Muon0, _recoEffMuon0, eventInfo);
		//        m_muonEfficiency->getDataEfficiency(*Muon1, _recoEffMuon1, eventInfo);
		//        //Info("execute()", "Muon0 pT = %.2f, Muon0 eta = %.2f, Efficiency = %.3f", Muon0->pt()/1e3, Muon0->eta(), _recoEffMuon0 );
		//        //Info("execute()", "Muon1 pT = %.2f, Muon1 eta = %.2f, Efficiency = %.3f", Muon1->pt()/1e3, Muon1->eta(), _recoEffMuon1 );

		m_Muon_Phi_hxOLD     ->push_back( phi_hxOLD);
		m_Muon_CosTheta_hxOLD->push_back( cosTheta_hxOLD);
		double phi_hx(0.);
		double cosTheta_hx(0.);
		if (Muon0_ptr->charge() > 0) {
			m_toolbox->VatoHelicity(Muon0_RefTrack,Muon1_RefTrack,
					cosTheta_hx,phi_hx);
		} else {
			m_toolbox->VatoHelicity(Muon1_RefTrack,Muon0_RefTrack,
					cosTheta_hx,phi_hx);
		}


		m_Muon_Phi_hx     ->push_back( phi_hx);
		m_Muon_CosTheta_hx->push_back( cosTheta_hx);



		m_Muon0_RefTrack_p4->push_back(Muon0_RefTrack);
		m_Muon0_Track_p4->push_back(Track0->p4());
		m_Muon0_CBTrack_p4->push_back(Muon0_ptr->p4());
		//m_Muon0_RefTrack_Pt->push_back(   Muon0_RefTrack.Pt());
		//m_Muon0_RefTrack_Eta->push_back(  Muon0_RefTrack.Eta());
		//m_Muon0_RefTrack_Phi->push_back(	 Muon0_RefTrack.Phi());
		//m_Muon0_RefTrack_Theta->push_back(Muon0_RefTrack.Theta());
		//m_Muon0_Track_Pt->push_back(   Track0->pt());
		//m_Muon0_Track_Eta->push_back(  Track0->eta());
		//m_Muon0_Track_Phi->push_back(  Track0->phi());
		//m_Muon0_Track_Theta->push_back(Track0->theta());
		//m_Muon0_Track_d0->push_back(   Track0->d0());
		//m_Muon0_Track_z0->push_back(   Track0->z0());
		//m_Muon0_CBTrack_Pt->push_back(   Muon0_ptr->pt());
		//m_Muon0_CBTrack_Eta->push_back(  Muon0_ptr->eta());
		//m_Muon0_CBTrack_Phi->push_back(  Muon0_ptr->phi());
		m_Muon0_Charge->push_back(	Muon0_ptr->charge());
		m_Muon0_Quality->push_back(	muon0_qual);
		//        m_Muon0_MatchdR_mu4->push_back(    trigMuonMatching->minDelR  (Muon0,"HLT_mu4",0.2));
		//        m_Muon0_MatchdR_mu4_trk->push_back(trigMuonMatching->minDelR  (Muon0,"HLT_mu4_bJpsi_Trkloose",0.2));
		//        m_Muon0_MatchdR_mu6->push_back(    trigMuonMatching->minDelR  (Muon0,"HLT_mu6",0.2));
		//        m_Muon0_MatchdR_mu6_trk->push_back(trigMuonMatching->minDelR  (Muon0,"HLT_mu6_bJpsi_Trkloose",0.2));
		//        m_Muon0_MatchdR_mu20->push_back(   trigMuonMatching->minDelR  (Muon0,"HLT_mu20_L1MU15",0.2));
		//        m_Muon0_MatchdR_mu20i->push_back(  trigMuonMatching->minDelR  (Muon0,"HLT_mu20_iloose_L1MU15",0.2));
		//        m_Muon0_MatchdR_MU4->push_back(    trigMuonMatching->minDelRL1(Muon0,"L1_MU4",1.));
		//        m_Muon0_MatchdR_MU6->push_back(    trigMuonMatching->minDelRL1(Muon0,"L1_MU6",1.));
		//        m_Muon0_MatchdR_MU15->push_back(   trigMuonMatching->minDelRL1(Muon0,"L1_MU15",1.));


		m_Muon1_RefTrack_p4->push_back(Muon1_RefTrack);
		m_Muon1_Track_p4->push_back(Track1->p4());
		m_Muon1_CBTrack_p4->push_back(Muon1_ptr->p4());
		//m_Muon1_RefTrack_Pt->push_back(   Muon1_RefTrack.Pt());
		//m_Muon1_RefTrack_Eta->push_back(  Muon1_RefTrack.Eta());
		//m_Muon1_RefTrack_Phi->push_back(  Muon1_RefTrack.Phi());
		//m_Muon1_RefTrack_Theta->push_back(Muon1_RefTrack.Theta());
		//m_Muon1_Track_Pt->push_back(   Track1->pt());
		//m_Muon1_Track_Eta->push_back(  Track1->eta());
		//m_Muon1_Track_Phi->push_back(  Track1->phi());
		//m_Muon1_Track_Theta->push_back(Track1->theta());
		//m_Muon1_Track_d0->push_back(   Track1->d0());
		//m_Muon1_Track_z0->push_back(   Track1->z0());
		//m_Muon1_CBTrack_Pt->push_back(   Muon1_ptr->pt());
		//m_Muon1_CBTrack_Eta->push_back(  Muon1_ptr->eta());
		//m_Muon1_CBTrack_Phi->push_back(  Muon1_ptr->phi());
		m_Muon1_Charge->push_back(       Muon1_ptr->charge());
		m_Muon1_Quality->push_back(	muon1_qual);
		//        m_Muon1_MatchdR_mu4->push_back(    trigMuonMatching->minDelR  (Muon1,"HLT_mu4",0.2));
		//        m_Muon1_MatchdR_mu4_trk->push_back(trigMuonMatching->minDelR  (Muon1,"HLT_mu4_bJpsi_Trkloose",0.2));
		//        m_Muon1_MatchdR_mu6->push_back(    trigMuonMatching->minDelR  (Muon1,"HLT_mu6",0.2));
		//        m_Muon1_MatchdR_mu6_trk->push_back(trigMuonMatching->minDelR  (Muon1,"HLT_mu6_bJpsi_Trkloose",0.2));
		//        m_Muon1_MatchdR_mu20->push_back(   trigMuonMatching->minDelR  (Muon1,"HLT_mu20_L1MU15",0.2));
		//        m_Muon1_MatchdR_mu20i->push_back(  trigMuonMatching->minDelR  (Muon1,"HLT_mu20_iloose_L1MU15",0.2));
		//        m_Muon1_MatchdR_MU4->push_back(    trigMuonMatching->minDelRL1(Muon1,"L1_MU4",1.));
		//        m_Muon1_MatchdR_MU6->push_back(    trigMuonMatching->minDelRL1(Muon1,"L1_MU6",1.));
		//        m_Muon1_MatchdR_MU15->push_back(   trigMuonMatching->minDelRL1(Muon1,"L1_MU15",1.));

		//        if (Muon0) { delete Muon0; Muon0 = NULL; }
		//        if (Muon1) { delete Muon1; Muon1 = NULL; }
	}

	//---------------------------
	// Loop inside Muons
	//---------------------------

	xAOD::MuonContainer::const_iterator mu0_itr = muons->begin();
	xAOD::MuonContainer::const_iterator mu0_end = muons->end();

	if (!m_MuonLoop) goto finish;
	for ( ; mu0_itr != mu0_end; ++mu0_itr ) {
		const xAOD::Muon* Mu0 = *mu0_itr;
		auto Muon0_ptr = m_toolbox->makeCalibMuon(Mu0);
		TLorentzVector Muon0_CB = Muon0_ptr->p4();

		int muon0_qual(0);
		bool muon0_loose(false), muon0_medium(false),muon0_tight(false), muon0_lowpt(false);
		bool bvar(false);

		if (m_toolbox->passMuonTight (Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 1;   muon0_tight =true;}
		if (m_toolbox->passMuonMedium(Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 10;  muon0_medium=true;}
		if (m_toolbox->passMuonLoose (Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 100; muon0_loose =true;}
		if (m_toolbox->passMuonLowPt (Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 1000;muon0_lowpt =true;}

		xAOD::MuonContainer::const_iterator mu1_itr = std::next(mu0_itr);
		for ( ; mu1_itr != mu0_end; ++mu1_itr ) {
			const xAOD::Muon* Mu1 = *mu1_itr;
			auto Muon1_ptr = m_toolbox->makeCalibMuon(Mu1);
			TLorentzVector Muon1_CB = Muon1_ptr->p4();
			TLorentzVector DiMuon = Muon0_CB + Muon1_CB;

			if (DiMuon.M()/1e3 > 4.2 || DiMuon.M()/1e3 < 2.5)
				continue;

			int muon1_qual(0);
			bool muon1_loose(false), muon1_medium(false),muon1_tight(false), muon1_lowpt(false);
			if (m_toolbox->passMuonTight (Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 1;   muon1_tight =true;}
			if (m_toolbox->passMuonMedium(Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 10;  muon1_medium=true;}
			if (m_toolbox->passMuonLoose (Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 100; muon1_loose =true;}
			if (m_toolbox->passMuonLowPt (Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 1000;muon1_lowpt =true;}

			//if ( (!muon0_medium || !muon1_medium) && (!muon0_lowpt || !muon1_lowpt) ) continue;

			const std::vector<const xAOD::IParticle*> vecIptls = {Muon0_ptr.get(),Muon1_ptr.get()};
			const double matchThreshold(0.01);
			int triggMatch = 0; //Dimuon matches trigger
			int mu0_triggMatch = 0; //Mu1 matches trigger
			int mu1_triggMatch = 0; //Mu2 matches trigger
			for(unsigned int iTrg(0); iTrg<m_triggerNames.size(); iTrg++){
				if(m_triggerNames[iTrg].second){    //Dimuon trigger
					triggMatch |= (m_toolbox->match(vecIptls, m_triggerNames[iTrg].first, matchThreshold, false) ? (int)pow(2,iTrg) : 0);
					//If matched, then obviously both muons match
					if( triggMatch & (int)pow(2,iTrg)){
						mu0_triggMatch |= (int)pow(2,iTrg);
						mu1_triggMatch |= (int)pow(2,iTrg);
					}
				}
				else{                                 //Single mu trigger
					bool mu0_match(0), mu1_match(0);
					mu0_match = (m_toolbox->match(*vecIptls[0], m_triggerNames[iTrg].first, matchThreshold, false) ? 1 : 0);
					mu1_match = (m_toolbox->match(*vecIptls[1], m_triggerNames[iTrg].first, matchThreshold, false) ? 1 : 0);
					triggMatch |= ((mu0_match || mu1_match) ? (int)pow(2,iTrg) : 0);
					if(mu0_match){
						mu0_triggMatch |= (int)pow(2,iTrg);
					}
					if(mu1_match){
						mu1_triggMatch |= (int)pow(2,iTrg);
					}
				}
			}

			m_mu_DiMuon_TriggMatch->push_back(triggMatch);
			m_mu_Muon0_TriggMatch->push_back(mu0_triggMatch);
			m_mu_Muon1_TriggMatch->push_back(mu1_triggMatch);

			float _recoEffMuon0 = 1;
			float _recoEffMuon1 = 1;

			double _triggEffMuon0 = 1;
			double _triggEffMuon1 = 1;

			// Muon 0
			_recoEffMuon0 = -1.;
			_triggEffMuon0 = -1.;
			if (muon0_tight) {
				auto muonEffSFTool = m_toolbox->muonEffSFTightTool();
				//auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
				auto corrCodeMu0Eff = m_toolbox->getMCPEfficiency(muonEffSFTool, *Muon0_ptr, _recoEffMuon0, eventInfo, isMC);
				if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu0Eff Tight " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " <<Muon0_ptr->phi());
				}

				auto muonTriggSFTool = m_toolbox->muonTriggSFTightTool();
				auto corrCodeMu0Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon0_ptr, _triggEffMuon0, "HLT_mu50", !isMC);
				if (corrCodeMu0Trigg != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu0Trigg Tight " << corrCodeMu0Trigg << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " <<Muon0_ptr->phi());
				}
			}
			m_mu_Muon0_RecoEffTight->push_back( _recoEffMuon0);
			m_mu_Muon0_mu50TriggEffTight->push_back( _triggEffMuon0);

			_recoEffMuon0 = -1.;
			_triggEffMuon0 = -1.;
			if (muon0_medium) {
				auto muonEffSFTool = m_toolbox->muonEffSFMediumTool();
				auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
				if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu0Eff Medium " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " << Muon0_ptr->phi());

				}

				auto muonTriggSFTool = m_toolbox->muonTriggSFMediumTool();
				auto corrCodeMu0Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon0_ptr, _triggEffMuon0, "HLT_mu50", !isMC);
				if (corrCodeMu0Trigg != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu0Trigg Medium " << corrCodeMu0Trigg << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " <<Muon0_ptr->phi());
				}
			}
			m_mu_Muon0_RecoEffMedium->push_back( _recoEffMuon0);
			m_mu_Muon0_mu50TriggEffMedium->push_back( _triggEffMuon0);

			_recoEffMuon0 = -1.;
			_triggEffMuon0 = -1.;
			if (muon0_loose) {
				auto muonEffSFTool = m_toolbox->muonEffSFLooseTool();
				auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
				if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu0Eff Loose " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " << Muon0_ptr->phi());
				}

				auto muonTriggSFTool = m_toolbox->muonTriggSFLooseTool();
				auto corrCodeMu0Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon0_ptr, _triggEffMuon0, "HLT_mu50", !isMC);
				if (corrCodeMu0Trigg != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu0Trigg Loose " << corrCodeMu0Trigg << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " <<Muon0_ptr->phi());
				}
			}
			m_mu_Muon0_RecoEffLoose->push_back( _recoEffMuon0);
			m_mu_Muon0_mu50TriggEffLoose->push_back( _triggEffMuon0);

			_recoEffMuon0 = -1.;
			if (muon0_lowpt && m_toolbox->muonEffSFLowPtTool()) {
				auto muonEffSFTool = m_toolbox->muonEffSFLowPtTool();
				auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
				if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu0Eff LowPt " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " << Muon0_ptr->phi());
				}
			}
			m_mu_Muon0_RecoEffLowPt->push_back( _recoEffMuon0);

			// Muon 1
			_recoEffMuon1 = -1.;
			_triggEffMuon1 = -1.;
			if (muon1_tight) {
				auto muonEffSFTool = m_toolbox->muonEffSFTightTool();
				auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
				if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu1Eff Tight " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
				}

				auto muonTriggSFTool = m_toolbox->muonTriggSFTightTool();
				auto corrCodeMu1Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon1_ptr, _triggEffMuon1, "HLT_mu50", !isMC);
				if (corrCodeMu1Trigg != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu1Trigg Tight " << corrCodeMu1Trigg << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " <<Muon1_ptr->phi());
				}
			}
			m_mu_Muon1_RecoEffTight->push_back( _recoEffMuon1);
			m_mu_Muon1_mu50TriggEffTight->push_back( _triggEffMuon1);

			_recoEffMuon1 = -1.;
			_triggEffMuon1 = -1.;
			if (muon1_medium) {
				auto muonEffSFTool = m_toolbox->muonEffSFMediumTool();
				auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
				if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu1Eff Medium " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
				}

				auto muonTriggSFTool = m_toolbox->muonTriggSFMediumTool();
				auto corrCodeMu1Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon1_ptr, _triggEffMuon1, "HLT_mu50", !isMC);
				if (corrCodeMu1Trigg != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu1Trigg Medium " << corrCodeMu1Trigg << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " <<Muon1_ptr->phi());
				}
			}
			m_mu_Muon1_RecoEffMedium->push_back( _recoEffMuon1);
			m_mu_Muon1_mu50TriggEffMedium->push_back( _triggEffMuon1);

			_recoEffMuon1 = -1.;
			_triggEffMuon1 = -1.;
			if (muon1_loose) {
				auto muonEffSFTool = m_toolbox->muonEffSFLooseTool();
				auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
				if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu1Eff Loose " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
				}

				auto muonTriggSFTool = m_toolbox->muonTriggSFLooseTool();
				auto corrCodeMu1Trigg = muonTriggSFTool->getTriggerEfficiency(*Muon1_ptr, _triggEffMuon1, "HLT_mu50", !isMC);
				if (corrCodeMu1Trigg != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu1Trigg Loose " << corrCodeMu1Trigg << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " <<Muon1_ptr->phi());
				}
			}
			m_mu_Muon1_RecoEffLoose->push_back( _recoEffMuon1);
			m_mu_Muon1_mu50TriggEffLoose->push_back( _triggEffMuon1);

			_recoEffMuon1 = -1.;
			if (muon1_lowpt && m_toolbox->muonEffSFLowPtTool()) {
				auto muonEffSFTool = m_toolbox->muonEffSFLowPtTool();
				auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
				if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
					ATH_MSG_DEBUG ("corrCodeMu1Eff LowPt " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
				}
			}
			m_mu_Muon1_RecoEffLowPt->push_back( _recoEffMuon1);

			double phi_hxOLD      = m_toolbox->phiMethod(Muon0_CB,Muon1_CB,Muon0_ptr->charge());
			double cosTheta_hxOLD = m_toolbox->cosMethod(Muon0_CB,Muon1_CB,Muon0_ptr->charge());

			m_mu_Muon_Phi_hxOLD     ->push_back( phi_hxOLD);
			m_mu_Muon_CosTheta_hxOLD->push_back( cosTheta_hxOLD);
			double phi_hx(0.);
			double cosTheta_hx(0.);
			if (Muon0_ptr->charge() > 0) {
				m_toolbox->VatoHelicity(Muon0_CB,Muon1_CB,
						cosTheta_hx,phi_hx);
			} else {
				m_toolbox->VatoHelicity(Muon1_CB,Muon0_CB,
						cosTheta_hx,phi_hx);
			}

			m_mu_Muon_Phi_hx     ->push_back( phi_hx);
			m_mu_Muon_CosTheta_hx->push_back( cosTheta_hx);

			m_mu_Muon0_CBTrack_p4->push_back(Muon0_ptr->p4());
			m_mu_Muon0_Charge->push_back(Muon0_ptr->charge());
			m_mu_Muon0_Quality->push_back(muon0_qual);

			m_mu_Muon1_CBTrack_p4->push_back(Muon1_ptr->p4());
			m_mu_Muon1_Charge->push_back(Muon1_ptr->charge());
			m_mu_Muon1_Quality->push_back(muon1_qual);

			m_mu_DiMuon_p4->push_back(DiMuon);
		}



	}

finish:

	////Save event only if at least one trigger was active or MC selection was passed
	//Save event only if ther is at least 1 candidate or MC selection was passed
	//if ( writeMC || (m_EventTrigger &&  m_DiMuon_Mass->size()) ) {
	if ( writeMC || m_DiMuon_Mass->size() || m_mu_DiMuon_p4->size()) {
		ATH_CHECK(fillTree());
	}

	setFilterPassed(true); //if got here, assume that means algorithm passed
	return StatusCode::SUCCESS;
}

StatusCode OniaAnaAlg::bookBranches () {
	m_myTree->Branch("RunNumber",   &m_RunNumber);
	m_myTree->Branch("EventNumber", &m_EventNumber);
	m_myTree->Branch("Pileup", &m_Pileup);
	m_myTree->Branch("LumiBlock",   &m_LumiBlock);
	m_myTree->Branch("mcChannelNumber",   &m_mcChannelNumber);

	m_myTree->Branch("EventTrigger", &m_EventTrigger);

	m_myTree->Branch("PS", &m_PS);

    m_myTree->Branch("Truth_Parent_PdgId",    &m_Truth_Parent_PdgId);
    m_myTree->Branch("Truth_Parent_Pt",       &m_Truth_Parent_Pt);
    m_myTree->Branch("Truth_Parent_Eta",      &m_Truth_Parent_Eta);
    m_myTree->Branch("Truth_Parent_Phi",      &m_Truth_Parent_Phi);
    m_myTree->Branch("Truth_Parent_Mass",     &m_Truth_Parent_Mass);
    m_myTree->Branch("Truth_Onia_PdgId",      &m_Truth_Onia_PdgId);
    m_myTree->Branch("Truth_Onia_Pt",         &m_Truth_Onia_Pt);
    m_myTree->Branch("Truth_Onia_Eta",        &m_Truth_Onia_Eta);
    m_myTree->Branch("Truth_Onia_Phi",        &m_Truth_Onia_Phi);
    m_myTree->Branch("Truth_Onia_Mass",       &m_Truth_Onia_Mass);
    m_myTree->Branch("Truth_Onia_Vertex_X",   &m_Truth_Onia_Vertex_X);
    m_myTree->Branch("Truth_Onia_Vertex_Y",   &m_Truth_Onia_Vertex_Y);
    m_myTree->Branch("Truth_Onia_Vertex_Z",   &m_Truth_Onia_Vertex_Z);
    m_myTree->Branch("Truth_MuMinus_Pt",      &m_Truth_MuMinus_Pt);
    m_myTree->Branch("Truth_MuMinus_Eta",     &m_Truth_MuMinus_Eta);
    m_myTree->Branch("Truth_MuMinus_Phi",     &m_Truth_MuMinus_Phi);
    m_myTree->Branch("Truth_MuPlus_Pt",       &m_Truth_MuPlus_Pt);
    m_myTree->Branch("Truth_MuPlus_Eta",      &m_Truth_MuPlus_Eta);
    m_myTree->Branch("Truth_MuPlus_Phi",      &m_Truth_MuPlus_Phi);
    m_myTree->Branch("Truth_PV_Z",            &m_Truth_PV_Z);
    m_myTree->Branch("Truth_PV_X",            &m_Truth_PV_X);
    m_myTree->Branch("Truth_PV_Y",            &m_Truth_PV_Y);

    m_myTree->Branch("PV_Z",                  &m_PV_Z);
    m_myTree->Branch("PV_X",                  &m_PV_X);
    m_myTree->Branch("PV_Y",                  &m_PV_Y);

	m_myTree->Branch("DiMuon_Mass",               &m_DiMuon_Mass);
	m_myTree->Branch("DiMuon_MassErr",            &m_DiMuon_MassErr);
	m_myTree->Branch("DiMuon_Pt",                 &m_DiMuon_Pt);
	m_myTree->Branch("DiMuon_Rapidity",           &m_DiMuon_Rapidity);
	m_myTree->Branch("DiMuon_ChiSq",              &m_DiMuon_ChiSq);
	m_myTree->Branch("DiMuon_Vertex_X",           &m_DiMuon_Vertex_X);
	m_myTree->Branch("DiMuon_Vertex_Y",           &m_DiMuon_Vertex_Y);
	m_myTree->Branch("DiMuon_Vertex_Z",           &m_DiMuon_Vertex_Z);
	m_myTree->Branch("Tau_ConstMass_MaxSumPt2",   &m_Tau_JpsiMass_MaxSumPt2);
	m_myTree->Branch("TauErr_ConstMass_MaxSumPt2",&m_TauErr_JpsiMass_MaxSumPt2);
	m_myTree->Branch("Tau_InvMass_MaxSumPt2",     &m_Tau_InvMass_MaxSumPt2);
	m_myTree->Branch("TauErr_InvMass_MaxSumPt2",  &m_TauErr_InvMass_MaxSumPt2);
	m_myTree->Branch("Tau_ConstMass_Min_A0",      &m_Tau_JpsiMass_Min_A0);
	m_myTree->Branch("TauErr_ConstMass_Min_A0",   &m_TauErr_JpsiMass_Min_A0);
	m_myTree->Branch("Tau_InvMass_Min_A0",        &m_Tau_InvMass_Min_A0);
	m_myTree->Branch("TauErr_InvMass_Min_A0",     &m_TauErr_InvMass_Min_A0);
	m_myTree->Branch("Tau_ConstMass_Min_Z0",      &m_Tau_JpsiMass_Min_Z0);
	m_myTree->Branch("TauErr_ConstMass_Min_Z0",   &m_TauErr_JpsiMass_Min_Z0);
	m_myTree->Branch("Tau_InvMass_Min_Z0",        &m_Tau_InvMass_Min_Z0);
	m_myTree->Branch("TauErr_InvMass_Min_Z0",     &m_TauErr_InvMass_Min_Z0);

	m_myTree->Branch("Lxy_MaxSumPt2",    &m_Lxy_MaxSumPt2);
	m_myTree->Branch("LxyErr_MaxSumPt2", &m_LxyErr_MaxSumPt2);
	m_myTree->Branch("Lxy_Min_A0",       &m_Lxy_Min_A0);
	m_myTree->Branch("LxyErr_Min_A0",    &m_LxyErr_Min_A0);
	m_myTree->Branch("Lxy_Min_Z0",       &m_Lxy_Min_Z0);
	m_myTree->Branch("LxyErr_Min_Z0",    &m_LxyErr_Min_Z0);

	m_myTree->Branch("DiMuon_TriggMatch", &m_DiMuon_TriggMatch);

	m_myTree->Branch("Muon_Phi_hx",          &m_Muon_Phi_hx);
	m_myTree->Branch("Muon_CosTheta_hx",     &m_Muon_CosTheta_hx);
	m_myTree->Branch("Muon_Phi_hxOLD",          &m_Muon_Phi_hxOLD);
	m_myTree->Branch("Muon_CosTheta_hxOLD",     &m_Muon_CosTheta_hxOLD);

	m_myTree->Branch("Muon0_RefTrack_p4" ,   &m_Muon0_RefTrack_p4);
	m_myTree->Branch("Muon0_Track_p4" ,      &m_Muon0_Track_p4);
	m_myTree->Branch("Muon0_CBTrack_p4" ,    &m_Muon0_CBTrack_p4);
	m_myTree->Branch("Muon0_Charge",         &m_Muon0_Charge);
	m_myTree->Branch("Muon0_Quality",        &m_Muon0_Quality);
	m_myTree->Branch("Muon0_TriggMatch",     &m_Muon0_TriggMatch);

	m_myTree->Branch("Muon1_RefTrack_p4" ,   &m_Muon1_RefTrack_p4);
	m_myTree->Branch("Muon1_Track_p4" ,      &m_Muon1_Track_p4);
	m_myTree->Branch("Muon1_CBTrack_p4" ,    &m_Muon1_CBTrack_p4);
	m_myTree->Branch("Muon1_Charge",         &m_Muon1_Charge);
	m_myTree->Branch("Muon1_Quality",        &m_Muon1_Quality);
	m_myTree->Branch("Muon1_TriggMatch",     &m_Muon1_TriggMatch);

	m_myTree->Branch("Muon0_RecoEffTight",        &m_Muon0_RecoEffTight );
	m_myTree->Branch("Muon1_RecoEffTight",        &m_Muon1_RecoEffTight );

	m_myTree->Branch("Muon0_RecoEffMedium",       &m_Muon0_RecoEffMedium);
	m_myTree->Branch("Muon1_RecoEffMedium",       &m_Muon1_RecoEffMedium);

	m_myTree->Branch("Muon0_RecoEffLoose",        &m_Muon0_RecoEffLoose  );
	m_myTree->Branch("Muon1_RecoEffLoose",        &m_Muon1_RecoEffLoose  );

	m_myTree->Branch("Muon0_RecoEffLowPt",        &m_Muon0_RecoEffLowPt  );
	m_myTree->Branch("Muon1_RecoEffLowPt",        &m_Muon1_RecoEffLowPt  );

	m_myTree->Branch("Muon0_mu50TriggEffTight",      &m_Muon0_mu50TriggEffTight  );
	m_myTree->Branch("Muon1_mu50TriggEffTight",      &m_Muon1_mu50TriggEffTight  );

	m_myTree->Branch("Muon0_mu50TriggEffMedium",      &m_Muon0_mu50TriggEffMedium );
	m_myTree->Branch("Muon1_mu50TriggEffMedium",      &m_Muon1_mu50TriggEffMedium );

	m_myTree->Branch("Muon0_mu50TriggEffLoose",      &m_Muon0_mu50TriggEffLoose  );
	m_myTree->Branch("Muon1_mu50TriggEffLoose",      &m_Muon1_mu50TriggEffLoose  );

	//For "custom" dimuons
	m_myTree->Branch("mu_DiMuon_p4",         &m_mu_DiMuon_p4);
	m_myTree->Branch("mu_DiMuon_TriggMatch", &m_mu_DiMuon_TriggMatch);

	m_myTree->Branch("mu_Muon_Phi_hx",          &m_mu_Muon_Phi_hx);
	m_myTree->Branch("mu_Muon_CosTheta_hx",     &m_mu_Muon_CosTheta_hx);
	m_myTree->Branch("mu_Muon_Phi_hxOLD",       &m_mu_Muon_Phi_hxOLD);
	m_myTree->Branch("mu_Muon_CosTheta_hxOLD",  &m_mu_Muon_CosTheta_hxOLD);

	m_myTree->Branch("mu_Muon0_CBTrack_p4" ,    &m_mu_Muon0_CBTrack_p4);
	m_myTree->Branch("mu_Muon0_Charge",         &m_mu_Muon0_Charge);
	m_myTree->Branch("mu_Muon0_Quality",        &m_mu_Muon0_Quality);
	m_myTree->Branch("mu_Muon0_TriggMatch",     &m_mu_Muon0_TriggMatch);

	m_myTree->Branch("mu_Muon1_CBTrack_p4" ,    &m_mu_Muon1_CBTrack_p4);
	m_myTree->Branch("mu_Muon1_Charge",         &m_mu_Muon1_Charge);
	m_myTree->Branch("mu_Muon1_Quality",        &m_mu_Muon1_Quality);
	m_myTree->Branch("mu_Muon1_TriggMatch",     &m_mu_Muon1_TriggMatch);

	m_myTree->Branch("mu_Muon0_RecoEffTight",        &m_mu_Muon0_RecoEffTight );
	m_myTree->Branch("mu_Muon1_RecoEffTight",        &m_mu_Muon1_RecoEffTight );

	m_myTree->Branch("mu_Muon0_RecoEffMedium",       &m_mu_Muon0_RecoEffMedium);
	m_myTree->Branch("mu_Muon1_RecoEffMedium",       &m_mu_Muon1_RecoEffMedium);

	m_myTree->Branch("mu_Muon0_RecoEffLoose",        &m_mu_Muon0_RecoEffLoose  );
	m_myTree->Branch("mu_Muon1_RecoEffLoose",        &m_mu_Muon1_RecoEffLoose  );

	m_myTree->Branch("mu_Muon0_RecoEffLowPt",        &m_mu_Muon0_RecoEffLowPt  );
	m_myTree->Branch("mu_Muon1_RecoEffLowPt",        &m_mu_Muon1_RecoEffLowPt  );

	m_myTree->Branch("mu_Muon0_mu50TriggEffTight",      &m_mu_Muon0_mu50TriggEffTight  );
	m_myTree->Branch("mu_Muon1_mu50TriggEffTight",      &m_mu_Muon1_mu50TriggEffTight  );

	m_myTree->Branch("mu_Muon0_mu50TriggEffMedium",      &m_mu_Muon0_mu50TriggEffMedium );
	m_myTree->Branch("mu_Muon1_mu50TriggEffMedium",      &m_mu_Muon1_mu50TriggEffMedium );

	m_myTree->Branch("mu_Muon0_mu50TriggEffLoose",      &m_mu_Muon0_mu50TriggEffLoose  );
	m_myTree->Branch("mu_Muon1_mu50TriggEffLoose",      &m_mu_Muon1_mu50TriggEffLoose  );

	return StatusCode::SUCCESS;

}
StatusCode OniaAnaAlg::clearBranches() {
	ATH_MSG_VERBOSE ("Line " << __LINE__);

	m_RunNumber        = 0;
	m_EventNumber      = 0;
	m_Pileup = 0;
	m_LumiBlock        = 0;
	m_mcChannelNumber  = 0;

	m_EventTrigger = 0;

	m_PS->clear();

	m_Truth_Parent_PdgId->clear();
	m_Truth_Parent_Pt->clear();
	m_Truth_Parent_Eta->clear();
	m_Truth_Parent_Phi->clear();
	m_Truth_Parent_Mass->clear();
	m_Truth_Onia_PdgId->clear();
	m_Truth_Onia_Pt->clear();
	m_Truth_Onia_Eta->clear();
	m_Truth_Onia_Phi->clear();
	m_Truth_Onia_Mass->clear();
	m_Truth_Onia_Vertex_X->clear();
	m_Truth_Onia_Vertex_Y->clear();
	m_Truth_Onia_Vertex_Z->clear();
	m_Truth_MuMinus_Pt->clear();
	m_Truth_MuMinus_Eta->clear();
	m_Truth_MuMinus_Phi->clear();
	m_Truth_MuPlus_Pt->clear();
	m_Truth_MuPlus_Eta->clear();
	m_Truth_MuPlus_Phi->clear();

	m_Truth_PV_Z = 0;
	m_Truth_PV_X = 0;
	m_Truth_PV_Y = 0;
	m_PV_Z = 0;
	m_PV_X = 0;
	m_PV_Y = 0;

	m_DiMuon_Mass->clear();
	m_DiMuon_Pt->clear();
	m_DiMuon_MassErr->clear();
	m_DiMuon_Rapidity->clear();
	m_DiMuon_ChiSq->clear();
	m_DiMuon_Vertex_X->clear();
	m_DiMuon_Vertex_Y->clear();
	m_DiMuon_Vertex_Z->clear();
	ATH_MSG_VERBOSE ("Line " << __LINE__);

	m_Tau_InvMass_MaxSumPt2     ->clear();
	m_Tau_JpsiMass_MaxSumPt2    ->clear();
	m_TauErr_InvMass_MaxSumPt2  ->clear();
	m_TauErr_JpsiMass_MaxSumPt2 ->clear();
	m_Tau_InvMass_Min_A0        ->clear();
	m_Tau_JpsiMass_Min_A0       ->clear();
	m_TauErr_InvMass_Min_A0     ->clear();
	m_TauErr_JpsiMass_Min_A0    ->clear();
	m_Tau_InvMass_Min_Z0        ->clear();
	m_Tau_JpsiMass_Min_Z0       ->clear();
	m_TauErr_InvMass_Min_Z0     ->clear();
	m_TauErr_JpsiMass_Min_Z0    ->clear();

	m_Lxy_MaxSumPt2    ->clear();
	m_LxyErr_MaxSumPt2 ->clear();
	m_Lxy_Min_A0       ->clear();
	m_LxyErr_Min_A0    ->clear();
	m_Lxy_Min_Z0       ->clear();
	m_LxyErr_Min_Z0    ->clear();

	ATH_MSG_VERBOSE ("Line " << __LINE__);

	m_DiMuon_TriggMatch->clear();

	/*
	m_DiMuon_IsMatch_2mu4_bUpsimumu->clear();
	m_DiMuon_IsMatch_2mu4_bJpsimumu->clear();
	m_DiMuon_IsMatch_2mu4_bUpsimumu_noL2->clear();
	m_DiMuon_IsMatch_2mu4_bJpsimumu_noL2->clear();
	m_DiMuon_IsMatch_2mu4_bDimu->clear();
	m_DiMuon_IsMatch_2mu4_bDimu_noinvm_novtx_ss->clear();
	m_DiMuon_IsMatch_2mu4_bDimu_novtx_noos->clear();
	m_DiMuon_IsMatch_mu6_mu4_bJpsimumu->clear();
	m_DiMuon_IsMatch_mu6_mu4_bUpsimumu->clear();
	m_DiMuon_IsMatch_mu6_mu4_bJpsimumu_noL2->clear();
	m_DiMuon_IsMatch_mu6_mu4_bUpsimumu_noL2->clear();
	m_DiMuon_IsMatch_2mu6_bUpsimumu->clear();
	m_DiMuon_IsMatch_2mu6_bJpsimumu->clear();
	m_DiMuon_IsMatch_2mu6_bUpsimumu_noL2->clear();
	m_DiMuon_IsMatch_2mu6_bJpsimumu_noL2->clear();
	m_DiMuon_IsMatch_2mu10_bUpsimumu->clear();
	m_DiMuon_IsMatch_2mu10_bJpsimumu->clear();
	m_DiMuon_IsMatch_2mu10_bUpsimumu_noL2->clear();
	m_DiMuon_IsMatch_2mu10_bJpsimumu_noL2->clear();
	m_DiMuon_IsMatch_mu20_iloose_L1MU15->clear();
	m_DiMuon_IsMatch_mu24_ivarmedium->clear();
	m_DiMuon_IsMatch_mu26_ivarmedium->clear();
	m_DiMuon_IsMatch_mu18_2mu0noL1_JpsimumuFS->clear();
	m_DiMuon_IsMatch_mu20_2mu0noL1_JpsimumuFS->clear();
	m_DiMuon_IsMatch_mu20_2mu2noL1_JpsimumuFS->clear();
	*/
	ATH_MSG_VERBOSE ("Line " << __LINE__);

	//m_Muon0_RecoEff->clear();
	m_Muon0_Charge->clear();
	//m_Muon0_Type->clear();
	//m_Muon0_PassedIDCuts->clear();
	//m_Muon0_isCombined->clear();
	m_Muon0_Track_d0->clear();
	m_Muon0_Track_z0->clear();
	m_Muon0_RefTrack_p4->clear();
	m_Muon0_Track_p4->clear();
	m_Muon0_CBTrack_p4->clear();
	//m_Muon0_RefTrack_Pt->clear();
	//m_Muon0_RefTrack_Eta->clear();
	//m_Muon0_RefTrack_Phi->clear();
	//m_Muon0_RefTrack_Theta->clear();
	//m_Muon0_Track_Pt->clear();
	//m_Muon0_Track_Eta->clear();
	//m_Muon0_Track_Phi->clear();
	//m_Muon0_Track_Theta->clear();
	//    m_Muon0_MeTrack_Pt->clear();
	//    m_Muon0_MeTrack_Eta->clear();
	//    m_Muon0_MeTrack_Phi->clear();
	//    m_Muon0_MeTrack_Theta->clear();
	//m_Muon0_CBTrack_Pt->clear();
	//m_Muon0_CBTrack_Eta->clear();
	//m_Muon0_CBTrack_Phi->clear();
	//    m_Muon0_CBTrack_Theta->clear();
	m_Muon0_Quality->clear();
	//    m_Muon0_qOverPsignif->clear();
	//    m_Muon0_qOverPsigma->clear();
	m_Muon0_TriggMatch->clear();
	//m_Muon0_MatchdR_mu4->clear();
	//m_Muon0_MatchdR_mu6->clear();
	//m_Muon0_MatchdR_mu4_trk->clear();
	//m_Muon0_MatchdR_mu6_trk->clear();
	//m_Muon0_MatchdR_mu20->clear();
	//m_Muon0_MatchdR_mu20i->clear();
	//m_Muon0_MatchdR_MU4->clear();
	//m_Muon0_MatchdR_MU6->clear();
	//m_Muon0_MatchdR_MU15->clear();
	ATH_MSG_VERBOSE ("Line " << __LINE__);


	//m_Muon1_RecoEff->clear();
	m_Muon1_Charge->clear();
	m_Muon_Phi_hx->clear();
	m_Muon_CosTheta_hx->clear();
	m_Muon_Phi_hxOLD->clear();
	m_Muon_CosTheta_hxOLD->clear();

	//    m_Muon1_Type->clear();
	//    m_Muon1_PassedIDCuts->clear();
	//    m_Muon1_isCombined->clear();
	m_Muon1_Track_d0->clear();
	m_Muon1_Track_z0->clear();
	m_Muon1_RefTrack_p4->clear();
	m_Muon1_Track_p4->clear();
	m_Muon1_CBTrack_p4->clear();
	//m_Muon1_RefTrack_Pt->clear();
	//m_Muon1_RefTrack_Eta->clear();
	//m_Muon1_RefTrack_Phi->clear();
	//m_Muon1_RefTrack_Theta->clear();
	//m_Muon1_Track_Pt->clear();
	//m_Muon1_Track_Eta->clear();
	//m_Muon1_Track_Phi->clear();
	//m_Muon1_Track_Theta->clear();
	//    m_Muon1_MeTrack_Pt->clear();
	//    m_Muon1_MeTrack_Eta->clear();
	//    m_Muon1_MeTrack_Phi->clear();
	//    m_Muon1_MeTrack_Theta->clear();
	//m_Muon1_CBTrack_Pt->clear();
	//m_Muon1_CBTrack_Eta->clear();
	//m_Muon1_CBTrack_Phi->clear();
	//    m_Muon1_CBTrack_Theta->clear();
	m_Muon1_Quality->clear();
	//    m_Muon1_qOverPsignif->clear();
	//    m_Muon1_qOverPsigma->clear();
	m_Muon1_TriggMatch->clear();
	//m_Muon1_MatchdR_mu4->clear();
	//m_Muon1_MatchdR_mu6->clear();
	//m_Muon1_MatchdR_mu4_trk->clear();
	//m_Muon1_MatchdR_mu6_trk->clear();
	//m_Muon1_MatchdR_MU4->clear();
	//m_Muon1_MatchdR_MU6->clear();
	//m_Muon1_MatchdR_MU15->clear();
	//m_Muon1_MatchdR_mu20->clear();
	//m_Muon1_MatchdR_mu20i->clear();

	m_Muon0_RecoEffTight ->clear();
	m_Muon1_RecoEffTight ->clear();
	m_Muon0_RecoEffMedium->clear();
	m_Muon1_RecoEffMedium->clear();
	m_Muon0_RecoEffLoose ->clear();
	m_Muon1_RecoEffLoose ->clear();
	m_Muon0_RecoEffLowPt ->clear();
	m_Muon1_RecoEffLowPt ->clear();

	m_Muon0_mu50TriggEffTight ->clear();
	m_Muon0_mu50TriggEffMedium->clear();
	m_Muon0_mu50TriggEffLoose ->clear();
	m_Muon1_mu50TriggEffTight ->clear();
	m_Muon1_mu50TriggEffMedium->clear();
	m_Muon1_mu50TriggEffLoose ->clear();

	m_mu_DiMuon_p4 ->clear();
	m_mu_DiMuon_TriggMatch ->clear();
	m_mu_Muon_Phi_hx ->clear();
	m_mu_Muon_CosTheta_hx ->clear();
	m_mu_Muon_Phi_hxOLD ->clear();
	m_mu_Muon_CosTheta_hxOLD ->clear();
	m_mu_Muon0_CBTrack_p4 ->clear();
	m_mu_Muon0_Charge ->clear();
	m_mu_Muon0_Quality ->clear();
	m_mu_Muon0_TriggMatch ->clear();
	m_mu_Muon1_CBTrack_p4 ->clear();
	m_mu_Muon1_Charge ->clear();
	m_mu_Muon1_Quality ->clear();
	m_mu_Muon1_TriggMatch ->clear();
	m_mu_Muon0_RecoEffTight  ->clear();
	m_mu_Muon1_RecoEffTight  ->clear();
	m_mu_Muon0_RecoEffMedium ->clear();
	m_mu_Muon1_RecoEffMedium ->clear();
	m_mu_Muon0_RecoEffLoose   ->clear();
	m_mu_Muon1_RecoEffLoose   ->clear();
	m_mu_Muon0_RecoEffLowPt   ->clear();
	m_mu_Muon1_RecoEffLowPt   ->clear();
	m_mu_Muon0_mu50TriggEffTight   ->clear();
	m_mu_Muon1_mu50TriggEffTight   ->clear();
	m_mu_Muon0_mu50TriggEffMedium  ->clear();
	m_mu_Muon1_mu50TriggEffMedium  ->clear();
	m_mu_Muon0_mu50TriggEffLoose   ->clear();
	m_mu_Muon1_mu50TriggEffLoose   ->clear();

	ATH_MSG_VERBOSE ("Line " << __LINE__);

	return StatusCode::SUCCESS;

}

StatusCode OniaAnaAlg::fillTree() {
	m_myTree->Fill();
	return StatusCode::SUCCESS;
}



StatusCode OniaAnaAlg::beginInputFile() {
	//
	//This method is called at the start of each input file, even if
	//the input file contains no events. Accumulate metadata information here
	//

	//example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
	// const xAOD::CutBookkeeperContainer* bks = 0;
	// CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

	//example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
	//float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
	//std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



	return StatusCode::SUCCESS;
}


