#!/bin/bash

while read line
do
  echo "Sumbitting: $line"
  year=$(echo $line | cut -d. -f1)
  run=$(echo $line | cut -d. -f2)
  stream=$(echo $line | cut -d. -f3)
  derivation=$(echo $line | cut -d. -f5)
  ami=$(echo $line | cut -d. -f6)
  date=$(date +%d_%m_%Y)
  #output="user.${USER}.${year}.${run}.${stream}.${derivation}.${ami}.${date}.v01.root"
  output="user.${USER}.${year}.${run}.${stream}.${derivation}.${date}.v01.root"
  echo "Output: ${output}"
  pathena OniaAna/OniaAnaAlgJobOptions.py --inDS $line --outDS $output --mergeOutput &
  echo "Done!"
  echo "------------------------------"
  sleep 4
done < $1
