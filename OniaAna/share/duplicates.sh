#!/bin/bash

#Print duplicate run numebers in input file list

#If same run number appears twice in row, it's considered as duplicate
#Usage: duplicates.sh input.txt

#Enhanced version: Streams should repeat too
#Usage: duplicates.sh -s input.txt

if [ "$1" = "-s" ]
then
	awk -F '.' '{if (a[$2]++ && s==$3){ print; print p;print "-----"; b[$3]=0;}{p=$0;s=$3}}' $2
elif [ "$1" = "-h" ]
then
	echo -e "Simple: If same run number appears twice in row, it's considered as duplicate\n"
	echo -e "\tduplicates.sh input.txt\n"
	echo -e "Enhanced: Streams should repeat too\n"
	echo -e "\tduplicates.sh -s input.txt\n"
else
	awk -F '.' 'a[$2]++{print; print p;print "-----"}{p=$0}' $1
fi
