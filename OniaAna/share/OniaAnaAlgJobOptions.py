#Skeleton joboption for a simple analysis job

#---- Minimal joboptions -------

#theApp.EvtMax=10                                         #how many events to run over. Set to -1 for all events.
#theApp.setOutputLevel(DEBUG)
jps.AthenaCommonFlags.FilesInput = ["DAOD_BPHY1.11512525._000086.pool.root.1"]   #insert your list of input files here (do this before next lines)

#Now choose your read mode (POOL, xAOD, or TTree):

#POOL:
#import AthenaPoolCnvSvc.ReadAthenaPool                   #sets up reading of any POOL files (but POOL is slow)

#xAOD:
import AthenaRootComps.ReadAthenaxAODHybrid               #FAST xAOD reading!

#TTree:
#import AthenaRootComps.ReadAthenaRoot                    #read a flat TTree, very fast, but no EDM objects
#svcMgr.EventSelector.TupleName="MyTree"                  #You usually must specify the name of the tree (default: CollectionTree)


#-------------------------------
algseq = CfgMgr.AthSequencer("AthAlgSeq")
subseq = CfgMgr.AthSequencer("SubSeq")
algseq += subseq


#note that if you edit the input files after this point you need to pass the new files to the EventSelector:
#like this: svcMgr.EventSelector.InputCollections = ["new","list"]

isSimulation = False
#from AthenaCommon.GlobalFlags import globalflags
#if globalflags.DataSource()=='geant4':
#    isSimulation = True

#retrieve data type from metadata of input file
from PyUtils import AthFile
af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
isSimulation = 'IS_SIMULATION' in af.fileinfos['evt_type']

#print isSimulation


##--------------------------------------------------------------------

if not isSimulation:
    ToolSvc += CfgMgr.GoodRunsListSelectionTool("GRLTool",PassThrough = True,
            GoodRunsListVec=["OniaAna/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
                "OniaAna/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
                "OniaAna/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
                "OniaAna/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
                ])
    subseq += CfgMgr.GRLSelectorAlg(Tool=ToolSvc.GRLTool)
    ## Adding LumiTree to the output
    svcMgr.MetaDataSvc.MetaDataTools += [ "LumiBlockMetaDataTool" ]
    theApp.CreateSvc += ['xAOD2NtupLumiSvc']

    print ToolSvc.GRLTool

year = af.infos['tag_info']['project_name'].split('_')[0].title()

#Configure MuonCalibrationAndSmearingTool
#Defaults for 2016
muCalibSmear_Year = year
muCalibSmear_StatComb = False
muCalibSmear_SagittaRelease = "sagittaBiasDataAll_03_02_19_Data16"
muCalibSmear_SagittaCorr = False
muCalibSmear_doSagittaMCDistortion = True
muCalibSmear_Release = "Recs2018_05_20"
muCalibSmear_SagittaCorrPhaseSpace = False

if year == 'Data15' or year == 'Is':
    muCalibSmear_Year = 'Data16'
elif year == 'Data17':
    muCalibSmear_Year = 'Data17'
    muCalibSmear_SagittaRelease = "sagittaBiasDataAll_03_02_19_Data17"
elif year == 'Data18':
    muCalibSmear_Year = 'Data18'
    muCalibSmear_SagittaRelease = "sagittaBiasDataAll_03_02_19_Data18"

#MC
if year == 'Is':
    ami = af.infos['tag_info']['AMITag']
    if 'r9364' in ami: #mc16a 2015/16
        muCalibSmear_Year = 'Data16'
    elif 'r10201' in ami: #mc16d 2017
        muCalibSmear_Year = 'Data17'
        muCalibSmear_SagittaRelease = "sagittaBiasDataAll_03_02_19_Data17"
    elif 'r10724' in ami: #mc16e 2018
        muCalibSmear_Year = 'Data18'
        muCalibSmear_SagittaRelease = "sagittaBiasDataAll_03_02_19_Data18"


ToolSvc += CfgMgr.CP__MuonCalibrationAndSmearingTool( "MuonCalibSmearTool",
        Year = muCalibSmear_Year, StatComb = muCalibSmear_StatComb,
        SagittaRelease = muCalibSmear_SagittaRelease,
        SagittaCorr = muCalibSmear_SagittaCorr,
        doSagittaMCDistortion = muCalibSmear_doSagittaMCDistortion,
        Release = muCalibSmear_Release,
        SagittaCorrPhaseSpace = muCalibSmear_SagittaCorrPhaseSpace,
        OutputLevel = INFO )
print ToolSvc.MuonCalibSmearTool

ToolSvc += CfgMgr.BLS__BLSToolBox(name="ToolBox", ToolBoxMuonCalibrationAndSmearingTool=ToolSvc.MuonCalibSmearTool, OutputLevel=VERBOSE)

subseq  += CfgMgr.OniaAnaAlg(OutputLevel=INFO,
        ToolBox=ToolSvc.ToolBox, Year = year,
        )                           #adds an instance of your alg to the main alg sequence


##--------------------------------------------------------------------
## This section shows up to set up a HistSvc output stream for outputing histograms and trees
## See https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_output_trees_and_histogra for more details and examples

if not hasattr(svcMgr, 'THistSvc'): svcMgr += CfgMgr.THistSvc() #only add the histogram service if not already present (will be the case in this jobo)
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"] #add an output root file stream

##--------------------------------------------------------------------
## The lines below are an example of how to create an output xAOD
## See https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_create_an_output_xAOD for more details and examples

#from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
#xaodStream = MSMgr.NewPoolRootStream( "StreamXAOD", "xAOD.out.root" )

##EXAMPLE OF BASIC ADDITION OF EVENT AND METADATA ITEMS
##AddItem and AddMetaDataItem methods accept either string or list of strings
#xaodStream.AddItem( ["xAOD::JetContainer#*","xAOD::JetAuxContainer#*"] ) #Keeps all JetContainers (and their aux stores)
#xaodStream.AddMetaDataItem( ["xAOD::TriggerMenuContainer#*","xAOD::TriggerMenuAuxContainer#*"] )
#ToolSvc += CfgMgr.xAODMaker__TriggerMenuMetaDataTool("TriggerMenuMetaDataTool") #MetaDataItems needs their corresponding MetaDataTool
#svcMgr.MetaDataSvc.MetaDataTools += [ ToolSvc.TriggerMenuMetaDataTool ] #Add the tool to the MetaDataSvc to ensure it is loaded

##EXAMPLE OF SLIMMING (keeping parts of the aux store)
#xaodStream.AddItem( ["xAOD::ElectronContainer#Electrons","xAOD::ElectronAuxContainer#ElectronsAux.pt.eta.phi"] ) #example of slimming: only keep pt,eta,phi auxdata of electrons

##EXAMPLE OF SKIMMING (keeping specific events)
#xaodStream.AddAcceptAlgs( "OniaAnaAlg" ) #will only keep events where 'setFilterPassed(false)' has NOT been called in the given algorithm

##--------------------------------------------------------------------


include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above
