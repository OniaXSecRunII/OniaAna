# OniaAna
Code to perform ntuple making for the 13 TeV J/psi production measurement in release 21.
Follows closely the format of the rel 20.7 code.

## Inputs
Takes BPHY1 derivation. Other derivations possible with change of the DimuonVertex key name

If not running in a derivation, then dimuon reconstruction needs to be run first.

## Outputs
Per-event root ntuple.

# Compiling and running the code
To checkout the code call

```bash
setupATLAS
lsetup git
mkdir OniaAnalysis
cd OniaAnalysis
git clone --recursive https://gitlab.cern.ch/OniaXSecRunII/OniaAna.git
mkdir build 
mkdir run

cd build
lsetup 'asetup AthAnalysis,21.2,latest,here' panda
cmake ../OniaAna
make -j
source */setup.sh
cd ../run

athena.py --filesInput DAOD_BPHY1.11512525._000086.pool.root.1 OniaAna/OniaAnaAlgJobOptions.py | tee out.txt

#To run on the grid:
GridRun.sh inputFileList.txt

#To find duplicate runs in input files list use duplicates.sh
#More details:
duplicates.sh -h

```
Note you may need to use the form  https://CERN_USERNAME:@gitlab or some other gitlab protocol (like ssh)

You can also replace *21.2,latest* for a stable release number.



